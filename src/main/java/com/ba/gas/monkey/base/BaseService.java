package com.ba.gas.monkey.base;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Data
@RequiredArgsConstructor
public abstract class BaseService<E extends BaseEntity, D extends IRequestBodyDTO> {

    private final BaseRepository<E> repository;
    private final ModelMapper modelMapper;

    public Page<D> getList(Pageable pageable) {
        Page<E> page = repository.findAll(pageable);
        List<D> list = convertForRead(page.getContent());
        return new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    }

    public D updateRowStatus(String id, Boolean status) {
        E obj = getById(id);
        obj.setStatus(status);
        obj.setDateModified(Instant.now());
        obj.setUpdtId(AuthenticationUtils.getTokenUserId());
        repository.save(obj);
        return convertForRead(getById(id));
    }

    protected List<D> convertForRead(List<E> e) {
        return e.stream().map(this::convertForRead).collect(Collectors.toList());
    }

    public D getByOid(@NonNull String id) {
        return convertForRead(getById(id));
    }

    protected E getById(@NonNull String id) {
        return getOptionalEntity(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB, "No " + getEntityClass().getSimpleName() + " Found with ID: " + id));
    }

    private Optional<E> getOptionalEntity(@NonNull String id) {
        return repository.findById(id);
    }

    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        return (Class<E>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    @SuppressWarnings("unchecked")
    private Class<D> getDtoClass() {
        return (Class<D>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1]);
    }

    public List<D> createAll(List<D> data) {
        List<E> entities = new ArrayList<>();
        E e;
        for (D dto : data) {
            e = convertForCreate(dto);
            e.setDateModified(Instant.now());
            e.setDateCreated(Instant.now());
            e.setUpdtId(AuthenticationUtils.getTokenUserId());
            entities.add(trimStringValues(e));
        }
        List<E> createdEntities = repository.saveAll(entities);
        return createdEntities.stream().map(this::convertForRead).collect(Collectors.toList());
    }

    public D create(E data) {
        data.setDateModified(Instant.now());
        data.setDateCreated(Instant.now());
        data.setUpdtId(AuthenticationUtils.getTokenUserId());
        E createdEntity = repository.save(data);
        return convertForRead(createdEntity);
    }

    public D update(String id, E data) {
        data.setId(id);
        data.setDateModified(Instant.now());
        data.setUpdtId(AuthenticationUtils.getTokenUserId());
        E createdEntity = repository.save(data);
        return convertForRead(createdEntity);
    }

    public D convertForRead(E e) {
        return modelMapper.map(e, getDtoClass());
    }

    protected E convertForCreate(D d) {
        E e = null;
        try {
            e = getEntityClass().newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
        BeanUtils.copyProperties(d, e);
        return e;
    }

    private E trimStringValues(E model) {
        for (Field field : model.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                Object value = field.get(model);
                String fieldName = field.getName();

                if (value != null) {
                    if (value instanceof String) {
                        String trimmed = (String) value;
                        field.set(model, trimmed.trim());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return model;
    }

}

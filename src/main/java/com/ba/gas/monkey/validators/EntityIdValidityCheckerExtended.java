package com.ba.gas.monkey.validators;


import com.ba.gas.monkey.base.BaseEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequestScope
@RequiredArgsConstructor
public class EntityIdValidityCheckerExtended implements ConstraintValidator<ValidEntityId, String> {

    private final RepositoryFactoryComponent repositoryFactoryComponent;

    private Class<? extends BaseEntity> entityClass;

    @Override
    public void initialize(ValidEntityId annotation) {
        entityClass = annotation.value();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s == null || repositoryFactoryComponent.getRepository(entityClass).findById(s).isPresent();
    }

}

package com.ba.gas.monkey.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumberValidation, String> {

    @Override
    public void initialize(PhoneNumberValidation contactNumber) {
    }

    //&& (contactField.length() > 10) && (contactField.length() < 15)

    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
        return contactField != null && contactField.matches("^\\+?(88)?0?1[23456789]\\d{8}\\b");
    }

}

package com.ba.gas.monkey.security.admin;

import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.services.user.UserInfoService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service("adminUserDetailsService")
public class AdminUserDetailsServiceImpl implements UserDetailsService {

    private final UserInfoService service;
    private final ModelMapper modelMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfoBean userInfoBean = service.getByEmailAddress(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + username));
        if (!userInfoBean.getStatus()) {
            throw new UsernameNotFoundException("Invalid user");
        }
        LoggedInUserBean userBean = modelMapper.map(userInfoBean, LoggedInUserBean.class);
        return new UserDetailsImpl(userBean);
    }

}

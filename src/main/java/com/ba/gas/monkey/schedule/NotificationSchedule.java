package com.ba.gas.monkey.schedule;

import com.ba.gas.monkey.models.Notification;
import com.ba.gas.monkey.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@EnableScheduling
@AllArgsConstructor
public class NotificationSchedule {

    private final NotificationWorker notificationWorker;
    private final NotificationService notificationService;

    @SneakyThrows
    @Scheduled(fixedDelay = 60 * 1000, initialDelay = 10 * 1000)
    public void sendNotify() {
        List<Notification> notificationList = notificationService.getUnSendNotificationList();
        notificationList = notificationList.stream().peek(notification -> notification.setNotificationStatus(-2)).collect(Collectors.toList());
        notificationService.getRepository().saveAll(notificationList);
        log.info("===========NotificationSchedule>>>>>>>>>>>> {}", notificationList.size());
        notificationList.forEach(notificationWorker::process);
    }
}

package com.ba.gas.monkey.schedule;

import com.ba.gas.monkey.dtos.firebase.Note;
import com.ba.gas.monkey.models.Notification;
import com.ba.gas.monkey.services.notification.AppNotificationService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class NotificationWorker {

    private final AppNotificationService appNotificationService;

    @SneakyThrows
    @Async("asyncExecutor")
    public void process(Notification notification) {

        Note note = appNotificationService.generatePromotionalNotificationNote(notification, "general");
        appNotificationService.sendPromotionalNotification(notification, note);
        log.info("===========NotificationPendingWorker>>>>>>>>>>>> {}", notification.getId());
        Thread.sleep(2 * 1000);
    }
}

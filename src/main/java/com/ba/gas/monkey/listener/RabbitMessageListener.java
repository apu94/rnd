package com.ba.gas.monkey.listener;


import com.ba.gas.monkey.config.RabbitConfig;
import com.ba.gas.monkey.dtos.firebase.TransactionMaster;
import com.ba.gas.monkey.models.Notification;
import com.ba.gas.monkey.repositories.NotificationRepository;
import com.ba.gas.monkey.services.notification.AMQPMessagePublisher;
import com.ba.gas.monkey.services.notification.NotificationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class RabbitMessageListener {

    public static final int UNABLE_TO_SEND_MESSAGE = -1;
    public static final int SUCCESSFULLY_SEND_MESSAGE = 1;
    private final ObjectMapper objectMapper;
    final private AMQPMessagePublisher amqpMessagePublisher;
    final private NotificationService notificationService;

    public RabbitMessageListener(ObjectMapper objectMapper, AMQPMessagePublisher amqpMessagePublisher, NotificationService notificationService) {
        this.objectMapper = objectMapper;
        this.amqpMessagePublisher = amqpMessagePublisher;
        this.notificationService = notificationService;
    }

    public void receiveMessageFinalAck(String message) throws JsonProcessingException {
        TransactionMaster transactionMaster = objectMapper.readValue(message, TransactionMaster.class);
        Optional<Notification> notification = ((NotificationRepository)notificationService.getRepository()).findById(transactionMaster.getNotificationId());
        if(notification.isPresent()){
            log.info("received at producer: " + transactionMaster);
            if(transactionMaster.getStatus() == UNABLE_TO_SEND_MESSAGE) {
                log.info("update get status == -1");
                notification.get().setNotificationStatus(-1);
                ((NotificationRepository)notificationService.getRepository()).save(notification.get());
            }else if(transactionMaster.getStatus() == SUCCESSFULLY_SEND_MESSAGE) {
                log.info("update get status == 1");
                notification.get().setNotificationStatus(1);
                ((NotificationRepository)notificationService.getRepository()).save(notification.get());
            }
        }

    }


    public void receiveMessageDeathQueue(String message) throws JsonProcessingException {
        TransactionMaster transactionMaster = objectMapper.readValue(message,  TransactionMaster.class);
        amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_FINALACK, transactionMaster);
    }
}

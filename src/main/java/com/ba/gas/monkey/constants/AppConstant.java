package com.ba.gas.monkey.constants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppConstant {

    public static final String TOKEN_TYPE = "Bearer";
    public static final long JWT_TOKEN_EXPIRE_IN = 24 * 3600000;
    public static final long REFRESH_TOKEN_EXPIRE_IN = 24 * 86400000;
    public static final String JWT_TOKEN_SECRET = "ind40ch3rn0bY1T3am";
    public static final Boolean ACTIVE_USER = Boolean.TRUE;

    public static final String ACTIVE = "Active";
    public static final String STATUS_YES = "Yes";
    public static final String STATUS_NO = "No";
    public static final String INACTIVE = "Inactive";
    public static final String JWT_TOKEN_ISSUER = "gas-monkey";
    public static final String JWT_TOKEN_SUBJECT = "JWT Token";
    public static final String ROLE_HEADER = "ROLE_";
    public static final String CUSTOMER_TYPE = "CUSTOMER";
    public static final String SUPER_ADMIN_EMAIL = "sadmin@batworld.com";
    public static final String[] IGNORE_PROPERTIES = {"id", "dateCreated"};
    public static final String USER_TYPE_ADMIN = "ADMIN";
    public static final String APP_OTP_LOGIN = "APP-OTP";
    public static final List<String> APP_USER_LIST = List.of("CUSTOMER", "PARTNER", "DEALER");
    public static final List<String> ORDER_COMPLETED_STATUS = List.of("COMPLETED", "DELIVERED", "RECEIVED");

    public static final List<String> ORDER_PENDING_STATUS = List.of("COMPLETED", "DELIVERED", "RECEIVED", "CANCELED");
    public static final String APP_MODULE = "APP";
    public static final String DEALER_TYPE = "DEALER";
    public static final String PARTNER_TYPE = "PARTNER";
    public static final Double DEFAULT_DISTANCE_FOR_SEARCH = 1.0;
    public static final String DEFAULT_PASSWORD = "12345678";
    public static final String NOT_APPROVED = "Not Approved";
    public static final String APPROVED = "Approved";
    /*exception-code*/
    public static final int KEY_CLOAK_TOKEN_EXCEPTION = 7001;
    public static final int NOT_FOUND_EXCEPTION_CODE = 9005;

    public static final int APP_USER_NOT_FOUND_EXCEPTION = 5001;
    public static final int APP_USER_TYPE_NOT_FOUND_EXCEPTION = 5002;
    public static final int TOKEN_EXPIRED_EXCEPTION = 8001;
    public static final int OTP_TOKEN_NOT_FOUND_EXCEPTION = 8002;
    /*exception-code*/
    /*map*/
    public static final Map<String, Object> SESSION_DATA_MAP = new HashMap<>();
    public static final String KEY_CLOAK_TOKEN = "KEY_CLOAK_TOKEN";
    public static final String KEY_CLOAK_TOKEN_EXPIRY = "KEY_CLOAK_TOKEN_EXPIRY";
    /*SMS Content*/
    public static final String OTP_TEMPLATE = "Your otp is $code. Gas Monkey Team";

    public static final String OTP_TYPE_SMS = "sms";
    public static final String OTP_TYPE_EMAIL = "email";

    public static final String SUCCESS = "success";
    public static final String USER_STATUS_PROCESSING = "processing";
    public static final String ORDER_STATUS_COMPLETED = "COMPLETED";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "Offline";
    public static final String NOTIFICATION_TYPE_PROMOTIONAL = "promotional";
    public static final String PUSH = "PUSH";
    public static final String SUPER_ADMIN = "SUPER_ADMIN";
    public static final String SUPPORT_TICKET_PREFIX = "ST-";
    public static final String DATE_FORMAT_DD_MM_YYYY = "dd-MMM-yyyy";
    public static final String PENDING = "Pending";
    public static final String PAID = "Paid";
    public static final Integer OTP_LENGTH = 4;
    public static final int PASSWORD_LENGTH = 8;
    public static final String EMPTY_STRING = "";
    public static final String CREATED = "CREATED";
    public static final String TIME_ZONE = "Asia/Dhaka";
    public static final String ACCOUNT_DELETE_MSG = "Your account deleted successfully";
    public static final String ORDER_TYPE_PACKAGE = "Package";
    public static final String ORDER_TYPE_REFILL = "Refill";
    public static final Map<String, String> ORDER_STATUS_MAP = new HashMap<>() {{
        put("created", "Order Received");
        put("pending", "Routing");
        put("accept", "Routing");
        put("picked", "Picked");
        put("received", "Received");
        put("delivered", "Delivered");
        put("reject", "Forwarded");
        put("nouserfound", "Partner Not Found");
        put("completed", "Empty Cylinder Return");
    }};

    public static final Map<String, String> ORDER_ACTION_MAP = new HashMap<>() {{
        put("created", "Received");
        put("reject", "Forward");
        put("accept", "Accepted");
        put("pending", "No Response");
    }};

    public static final Map<Integer, String> ORDER_STATUS_TAG_MAP = new HashMap<>() {{
        put(1, "First");
        put(2, "Second");
        put(3, "Third");
    }};

    public static final String SEARCH_AREA_IN_KM = "SEARCH_AREA_IN_KM";
}

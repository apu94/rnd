package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "valve_size")
@EqualsAndHashCode(callSuper = true)
public class ProductValveSize extends BaseEntity {
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_BN")
    private String nameBn;
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "product_image")
@EqualsAndHashCode(callSuper = true)
public class ProductImage extends BaseEntity {
    @Column(name = "TITLE")
    private String title;
    @Column(name = "IMAGE_LINK")
    private String imageLink;
    @Column(name = "IMAGE_LINK_ID")
    private String imageLinkId;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID")
    private Product product;
}

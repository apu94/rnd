package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "product")
@EqualsAndHashCode(callSuper = true)
public class Product extends BaseEntity {
    @Column(name = "CODE")
    private String code;
    @OneToOne
    @JoinColumn(name = "BRAND_ID", referencedColumnName = "ID")
    private Brand brand;
    @OneToOne
    @JoinColumn(name = "PRODUCT_SIZE_ID", referencedColumnName = "ID")
    private ProductSize productSize;
    @JsonManagedReference
    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private ProductPrice productPrice;
    @OneToOne
    @JoinColumn(name = "VALVE_SIZE_ID", referencedColumnName = "ID")
    private ProductValveSize productValveSize;
    @Column(name = "FEATURE_PRODUCT", columnDefinition = "BIT default 0", length = 1)
    private Boolean featureProduct=Boolean.FALSE;
    @Column(name = "OFFER_PRODUCT", columnDefinition = "BIT default 0", length = 1)
    private Boolean offerProduct=Boolean.FALSE;
    @JsonManagedReference
    @OneToMany(mappedBy = "product")
    private List<ProductImage> productImageList;
    @Column(name = "CONVENIENCE_FEE")
    private Double convenienceFee;

}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "transaction")
@EqualsAndHashCode(callSuper = true)
public class Transaction extends BaseEntity {
    @Column(name = "NOTE")
    private String note;
    @Column(name = "DEBIT", columnDefinition = " default '0.0'")
    private Double debit = 0.0;
    @Column(name = "CREDIT")
    private Double credit;
    @Column(name = "BALANCE")
    private Double balance;
    @OneToOne
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = "ID")
    private Customer partner;
}

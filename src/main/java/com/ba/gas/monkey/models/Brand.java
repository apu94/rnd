package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;


@Data
@Entity
@Table(name = "brand")
@EqualsAndHashCode(callSuper = true)
public class Brand extends BaseEntity {
    @JsonManagedReference
    @OneToMany(mappedBy = "brand")
    private List<BrandImage> brandImageList;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_BN")
    private String nameBn;
    @Column(name = "DISCOUNT_ENABLED", columnDefinition = "BIT default 0", length = 1)
    private Boolean discountEnabled=Boolean.FALSE;
    @Column(name = "DISCOUNT_TYPE", columnDefinition = "ENUM")
    private String discountType;
    @Column(name = "DISCOUNT_VALUE")
    private Double discountValue;
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "SHORT_NAME")
    private String shortName;
    @Column(name = "DISCOUNT_START_DATE")
    private LocalDate discountStartDate;
    @Column(name = "DISCOUNT_END_DATE")
    private LocalDate discountEndDate;
}

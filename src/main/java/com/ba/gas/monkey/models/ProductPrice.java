package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "product_price")
@EqualsAndHashCode(callSuper = true)
public class ProductPrice extends BaseEntity {
    @Column(name = "REFILL_PRICE")
    private Double refillPrice;
    @Column(name = "EMPTY_CYLINDER_PRICE")
    private Double emptyCylinderPrice;
    @Column(name = "PACKAGE_PRICE")
    private Double packagePrice;
    @JsonBackReference
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID")
    private Product product;
}

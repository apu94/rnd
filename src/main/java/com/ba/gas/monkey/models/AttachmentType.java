package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;


@Data
@Entity
@Table(name = "attachment_type")
@EqualsAndHashCode(callSuper = true)
public class AttachmentType extends BaseEntity {
    @Column(name = "ATTACHMENT_NAME")
    private String attachmentName;
}

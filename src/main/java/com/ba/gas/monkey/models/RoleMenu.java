package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "role_menu")
@EqualsAndHashCode(callSuper = true)
public class RoleMenu extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID")
    private GroupInfo groupInfo;
    @OneToOne
    @JoinColumn(name = "MENU_ID", referencedColumnName = "ID")
    private AppMenu appMenu;
}

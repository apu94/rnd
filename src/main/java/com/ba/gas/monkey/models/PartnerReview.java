package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "partner_review")
@EqualsAndHashCode(callSuper = true)
public class PartnerReview extends BaseEntity {

    @Column(name = "REVIEW_DATE")
    private LocalDateTime reviewDate;
    @Column(name = "REVIEWS_RATING")
    private Double reviewRating;
    @Column(name = "REVIEWS_READ")
    private Boolean reviewRead;
    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID")
    private OrderInfo orderInfo;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    private Customer customer;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = "ID")
    private Customer partner;
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "service_discount_offer")
@EqualsAndHashCode(callSuper = true)
public class ServiceDiscount extends BaseEntity {

    @Column(name = "DISCOUNT_ENABLED", columnDefinition = "BIT default 0", length = 1)
    private Boolean discountEnable;
    @Column(name = "DISCOUNT_TYPE", columnDefinition = "ENUM")
    private String discountType;
    @Column(name = "DISCOUNT_VALUE")
    private Double discountValue;
    @Column(name = "DISCOUNT_START_DATE")
    private LocalDate discountStartDate;
    @Column(name = "DISCOUNT_END_DATE")
    private LocalDate discountEndDate;

}

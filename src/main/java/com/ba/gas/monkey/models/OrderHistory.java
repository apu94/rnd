package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_history")
@EqualsAndHashCode(callSuper = true)
public class OrderHistory extends BaseEntity {

    @Column(name = "ORDER_STATUS", columnDefinition = "ENUM")
    private String orderStatus;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = "ID")
    private Customer partner;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID")
    private OrderInfo orderInfo;
    @Column(name = "SL_NO")
    private Integer slNo;

}

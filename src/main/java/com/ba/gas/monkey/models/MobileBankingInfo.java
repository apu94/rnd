package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;


@Data
@Entity
@Table(name = "mobile_banking_info")
@EqualsAndHashCode(callSuper = true)
public class MobileBankingInfo extends BaseEntity {
    @Column(name = "NAME")
    private String name;
}

package com.ba.gas.monkey.paylod.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseBean {
    private String msg;
}

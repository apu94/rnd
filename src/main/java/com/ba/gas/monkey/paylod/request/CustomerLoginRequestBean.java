package com.ba.gas.monkey.paylod.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CustomerLoginRequestBean {
    @NotBlank
    private String phoneNo;
    @NotBlank
    private String password;
}

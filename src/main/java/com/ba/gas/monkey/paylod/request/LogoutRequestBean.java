package com.ba.gas.monkey.paylod.request;

import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class LogoutRequestBean {
    @ValidEntityId(UserInfo.class)
    private String userId;
}

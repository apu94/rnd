package com.ba.gas.monkey.utility;

public class Constant {
    public static final String CREATED = "created";
    public static String PENDING = "pending";
    public static String NO_ACTION = "noaction";
    public static String NO_USER_FOUND = "nouserfound";
    public static String PICKED = "picked";
    public static String ACCEPT = "accept";
    public static String REJECT = "reject";
    public static String COMPLETED = "completed";
    public static String DELIVERED = "delivered";
    public static String RECEIVED = "received";
    public static String FORWARD = "forward";
    public static String CANCELED = "canceled";
    public static Integer TTL = 3;
    public static String ORDER = "order";
    public static String PAYMENT = "payment";
    public static String PROMOTIONAL = "promotional";
    public static String SCHEDULE = "schedule";
    public static String SUPPORT = "support";
    public static String CUSTOMER_ORDER_CONFIRMATION = "customer_order_confirmation";
    public static String SEND_ORDER_ACTION = "send_order_action";
    public static String DEALER_EMPTY_CYLINDER = "dealer_empty_cylinder";
    public static String DEALER_ORDER_PICKED = "dealer_order_picked";
    public static String PARTNER_ORDER_RECEIVED = "partner_order_received";
    public static String CUSTOMER_ORDER_PICKED = "customer_order_picked";
    public static String CUSTOMER_ORDER_DELIVERED = "customer_order_delivered";
    public static String CUSTOMER_ORDER_CANCELED = "customer_order_canceled";
    public static String PARTNER_ORDER_CANCELED = "partner_order_canceled";
    public static String ADMIN_PARTNER_NOT_FOUND= "admin_partner_not_found";
    public static String PARTNER = "partner";
    public static String CUSTOMER = "customer";
    public static String ADMIN = "admin";
    public static String DEALER = "dealer";
    public static String DEVICE_TYPE_ANDROID = "ANDROID";
    public static String DEVICE_TYPE_IOS = "IOS";
    public static String DEVICE_TYPE_ADMIN = "ADMIN";
    public static String COUPON_USE_TYPE_SERVICE = "SERVICE";
    public static String COUPON_USE_TYPE_ORDER = "ORDER";
    public static String DISCOUNT_TYPE_AMOUNT = "AMOUNT";
    public static String DISCOUNT_TYPE_PERCENT = "PERCENT";
}

package com.ba.gas.monkey.utility;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service("passwordUtils")
public class PasswordUtils {
    private PasswordEncoder passwordEncoder;

    public String getDefaultPassword() {
        return passwordEncoder.encode(AppConstant.DEFAULT_PASSWORD);
    }

    public String getHashPassword(String password) {
        return passwordEncoder.encode(password);
    }

    public String randomPassword() {
//        RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder()
//                .withinRange('1', 'Z')
//                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
//                .build();
//
//        return  randomStringGenerator.generate(AppConstant.PASSWORD_LENGTH).toUpperCase();
//        return RandomStringUtils.randomAlphanumeric(AppConstant.PASSWORD_LENGTH).toUpperCase();
        return RandomStringUtils.random(AppConstant.PASSWORD_LENGTH, Boolean.TRUE, Boolean.TRUE).toUpperCase();
    }
}

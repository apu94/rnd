package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.SliderImage;

public interface SliderImageRepository extends BaseRepository<SliderImage>  {

}

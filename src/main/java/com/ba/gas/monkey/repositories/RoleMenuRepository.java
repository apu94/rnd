package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.AppMenu;
import com.ba.gas.monkey.models.GroupInfo;
import com.ba.gas.monkey.models.RoleMenu;

import java.util.List;

public interface RoleMenuRepository extends BaseRepository<RoleMenu> {
    List<RoleMenu> findByGroupInfo(GroupInfo groupInfo);
}

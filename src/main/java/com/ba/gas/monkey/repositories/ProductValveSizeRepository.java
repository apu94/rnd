package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ProductValveSize;

public interface ProductValveSizeRepository extends BaseRepository<ProductValveSize> {
}

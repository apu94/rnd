package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ThanaRepository extends BaseRepository<Thana> {

    List<Thana> findByDistrict(District district);

    Page<Thana> findByDistrict(District district, Pageable page);
}

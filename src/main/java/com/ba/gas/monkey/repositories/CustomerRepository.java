package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.CustomerType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends BaseRepository<Customer> {


    String HAVERSINE_FORMULA = "(6371 * acos(cos(radians(:latitude)) * cos(radians(s.latitude)) *" +
            " cos(radians(s.longitude) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(s.latitude))))";

    String DISTANCE = "3956 * 2 * ASIN(\n" +
            "          SQRT( POWER(SIN((:latitude - abs(s.latitude)) * pi()/180 / 2), 2) \n" +
            "              + COS(:longitude * pi()/180 ) * COS(abs(s.latitude) * pi()/180)  \n" +
            "              * POWER(SIN((:longitude - s.longitude) * pi()/180 / 2), 2) ))";

    @Query("SELECT s FROM Customer s WHERE s.status=1 AND s.approved=1 AND s.userStatus='active' AND s.customerType.id=:typeId AND" + HAVERSINE_FORMULA + " < :distance ORDER BY " + HAVERSINE_FORMULA + " DESC")
    List<Customer> findWithInDistance(@Param("typeId") String typeId, @Param("latitude") double latitude, @Param("longitude") double longitude, @Param("distance") double distanceWithInKM);

    @Query(value = "SELECT * FROM customer c WHERE RIGHT(c.PHONE_NO, 11) =:phoneNo", nativeQuery = true)
    Optional<Customer> findByPhoneNo(String phoneNo);

    Page<Customer> findByCustomerType(CustomerType customerType, Pageable pageable);

    @Query("SELECT s FROM Customer s LEFT JOIN DeviceToken d ON d.userId = s.id WHERE s.status=1 AND s.approved=1 AND s.userStatus='active' AND s.customerType.id=:typeId AND d.token IS NOT NULL AND " + HAVERSINE_FORMULA + " < :distance ORDER BY " + HAVERSINE_FORMULA)
    Page<Customer> findByDistance(@Param("typeId") String typeId, @Param("latitude") double latitude, @Param("longitude") double longitude, @Param("distance") double distanceWithInKM, Pageable pageable);


    @Query("SELECT s FROM Customer s LEFT JOIN DeviceToken d ON d.userId = s.id WHERE s.status=1 AND s.approved=1 AND s.userStatus='active' AND s.customerType.id=:typeId AND s.id NOT IN (:ids) AND d.token IS NOT NULL AND " + HAVERSINE_FORMULA + " < :distance ORDER BY " + HAVERSINE_FORMULA)
    Page<Customer> findByDistanceFilterList(@Param("typeId") String typeId, @Param("latitude") double latitude, @Param("longitude") double longitude, @Param("distance") double distanceWithInKM, List<String> ids, Pageable pageable);


    @Query("SELECT s," + DISTANCE + " as distance FROM Customer s LEFT JOIN DeviceToken d ON d.userId = s.id WHERE s.status=1 AND s.approved=1 AND s.userStatus='active' AND s.customerType.id=:typeId AND d.token IS NOT NULL AND " + HAVERSINE_FORMULA + " < :distance ORDER BY " + HAVERSINE_FORMULA)
    Page<Object[]> findAllByDistance(@Param("typeId") String typeId, @Param("latitude") double latitude, @Param("longitude") double longitude, @Param("distance") double distanceWithInKM, Pageable pageable);

    @Query(value = "SELECT c FROM Customer c WHERE c.status=1 AND c.approved=1 AND c.userStatus='active' AND c.cluster.id=:clusterId AND c.customerType=:customerType")
    List<Customer> getByClusterIdAndCustomerType(String clusterId, CustomerType customerType);

    @Query(value = "SELECT c FROM Customer c WHERE c.status=1 AND c.approved=1 AND c.userStatus='active' AND c.customerType.id=:typeId")
    List<Customer> getByTypeId(String typeId);


    @Query("SELECT s FROM Customer s WHERE s.status=1 AND s.approved=1 AND s.userStatus='active' AND s.customerType.id=:typeId ORDER BY " + HAVERSINE_FORMULA + " ASC")
    List<Customer> findAllActiveDealer(@Param("typeId") String typeId, @Param("latitude") double latitude, @Param("longitude") double longitude);

}

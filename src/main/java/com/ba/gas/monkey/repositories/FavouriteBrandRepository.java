package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.BrandImage;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.FavouriteBrand;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FavouriteBrandRepository extends BaseRepository<FavouriteBrand> {

    Optional<FavouriteBrand> findByBrandAndCustomer(Brand brand, Customer customer);

    @Query(value = "SELECT f FROM FavouriteBrand f WHERE f.customer.id=:id AND f.status=1")
    List<FavouriteBrand> findByCustomerId(String id);
}

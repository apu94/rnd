package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.RefreshToken;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RefreshTokenRepository extends BaseRepository<RefreshToken> {
    @Query("SELECT r FROM RefreshToken r WHERE r.token=:token")
    Optional<RefreshToken> findByToken(String token);

}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.DealerBrand;

import java.util.List;

public interface DealerBrandRepository extends BaseRepository<DealerBrand>  {

    List<DealerBrand> findByDealerId(String id);
}

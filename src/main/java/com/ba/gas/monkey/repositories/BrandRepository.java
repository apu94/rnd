package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BrandRepository extends BaseRepository<Brand> {
    @Query(value = "SELECT b FROM Brand b WHERE lower(b.nameEn) LIKE %:text%")
    List<Brand> search(String text);
}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.NotificationType;

import java.util.Optional;

public interface NotificationTypeRepository extends BaseRepository<NotificationType> {
    Optional<NotificationType> findByName(String name);
}

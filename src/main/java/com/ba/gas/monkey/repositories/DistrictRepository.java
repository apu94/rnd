package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.BrandImage;
import com.ba.gas.monkey.models.District;

public interface DistrictRepository extends BaseRepository<District>  {

}

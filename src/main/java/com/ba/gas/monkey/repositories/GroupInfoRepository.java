package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.GroupInfo;

import java.util.List;
import java.util.Optional;

public interface GroupInfoRepository extends BaseRepository<GroupInfo> {
    Optional<GroupInfo> findByGroupType(String groupType);
}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Template;

import java.util.Optional;

public interface TemplateRepository extends BaseRepository<Template> {
    Optional<Template> findByAppToUser(String appToUser);
}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.CustomerAttachment;
import com.ba.gas.monkey.models.CustomerType;

import java.util.List;

public interface CustomerAttachmentRepository extends BaseRepository<CustomerAttachment> {

    List<CustomerAttachment> findByCustomerId(String customerId);
}

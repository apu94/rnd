package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderInfo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends BaseRepository<OrderInfo> {
    @Query("SELECT p FROM OrderInfo p WHERE p.customer.id=:id")
    List<OrderInfo> findByCustomerId(String id, Sort sort);

    @Query("SELECT p FROM OrderInfo p WHERE p.partner.id=:id")
    List<OrderInfo> findByPartnerId(String id, Sort sort);

    List<OrderInfo> findByDealerAndOrderStatus(Customer dealer, String orderStatus, Sort sort);

    List<OrderInfo> findByDealer(Customer dealer, Sort sort);

    @Query("SELECT p FROM OrderInfo p WHERE p.partner.id=:id")
    List<OrderInfo> findByPartnerId(String id);

    @Query("SELECT p FROM OrderInfo p WHERE p.customer.id=:id")
    List<OrderInfo> findByCustomerId(String id);

}

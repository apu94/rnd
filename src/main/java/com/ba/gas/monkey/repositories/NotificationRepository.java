package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface NotificationRepository extends BaseRepository<Notification> {
    List<Notification> findByReceiverId(String receiverId, Pageable pageable);
    List<Notification> findFirst15ByNotificationStatus(Integer notificationStatus, Sort sort);
}

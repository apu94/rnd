package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.DeviceToken;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DeviceTokenRepository extends BaseRepository<DeviceToken> {

    Optional<DeviceToken> findByUserIdAndDeviceType(String userId, String deviceType);

    List<DeviceToken> findByUserId(String userId);

    @Query(value = "SELECT d FROM DeviceToken d WHERE d.userId in (:ids)")
    List<DeviceToken> findByUserIds(List<String> ids);

    List<DeviceToken> findAllByUserIdAndDeviceType(String userId, String deviceType);
}

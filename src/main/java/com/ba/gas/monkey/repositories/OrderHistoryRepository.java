package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderHistory;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.projection.OrderHistoryProjectionBean;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrderHistoryRepository extends BaseRepository<OrderHistory> {

    Optional<OrderHistory> findByOrderInfoAndPartner(OrderInfo orderInfo, Customer partner);


    @Query("SELECT o FROM OrderHistory o WHERE o.orderInfo=:orderInfo ORDER BY o.slNo, o.dateModified")
    List<OrderHistory> findByOrderInfo(OrderInfo orderInfo);

    List<OrderHistory> findByPartner(Customer partner);


    @Query(value = "SELECT \n" +
            "CASE WHEN TAB.order_status='Routing' AND TAB.nsl=1 AND TAB.order_action ='No Response' THEN 'First Routing'\n" +
            "WHEN TAB.order_status='Routing' AND TAB.nsl=2 AND TAB.order_action ='No Response' THEN 'Second Routing'\n" +
            "WHEN TAB.order_status='Routing' THEN 'Last Routing'\n" +
            "ELSE TAB.order_status END AS orderStatus,\n" +
            "TAB.order_action AS orderAction, TAB.DATE_MODIFIED AS actionTime, TAB.response AS response\n" +
            "FROM ( SELECT row_number() over (PARTITION BY ORDER_STATUS\n" +
            "       ORDER BY DATE_MODIFIED) AS nsl, \n" +
            "CASE oh.ORDER_STATUS WHEN 'created' THEN 'Order Recieved' \n" +
            "WHEN 'pending' THEN 'Routing' \n" +
            "WHEN 'accept' THEN 'Routing' \n" +
            "WHEN 'picked' THEN 'Picked' \n" +
            "WHEN 'received' THEN 'Received' \n" +
            "WHEN 'delivered' THEN 'Delivered' \n" +
            "WHEN 'completed' THEN 'Empty Cylinder Return' \n" +
            "ELSE '' END AS order_status,\n" +
            "CASE oh.ORDER_STATUS \n" +
            "WHEN 'created' THEN 'Recieved' \n" +
            "WHEN 'reject' THEN 'Forward' \n" +
            "WHEN 'accept' THEN 'Accepted' \n" +
            "WHEN 'pending' THEN 'No Response' \n" +
            "ELSE '' END AS order_action, \n" +
            " oh.DATE_MODIFIED, \n" +
            "IFNULL(cus.CUSTOMER_NAME,'GMBL') AS response\n" +
            "FROM order_history oh\n" +
            "LEFT JOIN customer cus ON cus.ID=oh.PARTNER_ID\n" +
            "WHERE oh.ORDER_ID=:orderId\n" +
            "ORDER BY oh.SL_NO, oh.DATE_MODIFIED \n" +
            ") AS TAB", nativeQuery = true)
    List<OrderHistoryProjectionBean> getOrderHistoryById(String orderId);
}

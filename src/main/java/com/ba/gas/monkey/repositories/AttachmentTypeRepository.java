package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.AttachmentType;

public interface AttachmentTypeRepository extends BaseRepository<AttachmentType>  {

}

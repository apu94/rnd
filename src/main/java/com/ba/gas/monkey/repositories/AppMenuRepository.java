package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.AppMenu;

public interface AppMenuRepository extends BaseRepository<AppMenu> {
}

package com.ba.gas.monkey.resources.customer.product;

import com.ba.gas.monkey.dtos.product.ProductCompareBean;
import com.ba.gas.monkey.dtos.product.ProductDetailFilterBean;
import com.ba.gas.monkey.dtos.product.ProductFilterBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.product.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
public class CustomerProductResource {

    private final ProductService service;

    @GetMapping("offer-product")
    public ResponseEntity<?> getOfferProductList() {
        return new ResponseEntity<>(service.getOfferedProductList(), HttpStatus.OK);
    }

    @GetMapping("feature-product")
    public ResponseEntity<?> getFeatureProductList() {
        return new ResponseEntity<>(service.getFeatureProductList(), HttpStatus.OK);
    }

    @GetMapping("all-product")
    public ResponseEntity<?> getAllProductList() {
        return new ResponseEntity<>(service.getAllProductList(), HttpStatus.OK);
    }

    @GetMapping("product/{id}")
    public ResponseEntity<?> productDetails(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getProductById(id), HttpStatus.OK);
    }

    @PostMapping("product-detail-filter")
    public ResponseEntity<?> productDetailsFilter(@RequestBody @Valid ProductDetailFilterBean productDetailFilterBean) {
        return new ResponseEntity<>(service.getProductDetailsFilter(productDetailFilterBean), HttpStatus.OK);
    }

    @PostMapping("product-filter")
    public ResponseEntity<?> productFilter(@RequestBody @Valid ProductFilterBean productFilterBean) {
        return new ResponseEntity<>(service.getProductFilter(productFilterBean), HttpStatus.OK);
    }

    @PostMapping("product-compare")
    public ResponseEntity<?> productCompare(@RequestBody @Valid ProductCompareBean productCompareBean) {
        return new ResponseEntity<>(service.productCompare(productCompareBean), HttpStatus.OK);
    }

    @GetMapping("popular-product")
    public ResponseEntity<?> getPopularProductList() {
        return new ResponseEntity<>(service.getPopularProductList(), HttpStatus.OK);
    }

}

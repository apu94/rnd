package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.attachment.AttachmentTypeBean;
import com.ba.gas.monkey.services.AppMenuService;
import com.ba.gas.monkey.services.AttachmentTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class AppMenuResource {

    private final AppMenuService service;

    @GetMapping("app-menu")
    public ResponseEntity<?> getMenuList() {
        return new ResponseEntity<>(service.getMenuList(), HttpStatus.OK);
    }

}

package com.ba.gas.monkey.resources.admin.coupon;

import com.ba.gas.monkey.dtos.coupon.CouponBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class AdminCouponResource {
    private final CouponService service;

    @GetMapping("coupon")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getCouponList(pageable), HttpStatus.OK);
    }

    @DeleteMapping("coupon/{id}")
    public ResponseEntity<?> deleteCoupon(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.deleteByOid(id), HttpStatus.OK);
    }

    @PostMapping("coupon")
    public ResponseEntity<?> createCoupon(@RequestBody @Valid CouponBean bean) {
        return new ResponseEntity<>(service.createCoupon(bean), HttpStatus.OK);
    }

    @PutMapping("coupon/{couponId}")
    public ResponseEntity<?> updateCoupon(@PathVariable String couponId, @RequestBody @Valid CouponBean bean) {
        return new ResponseEntity<>(service.updateCoupon(couponId, bean), HttpStatus.OK);
    }

    @GetMapping("coupon/{id}")
    public ResponseEntity<?> getById(@PathVariable String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }
}

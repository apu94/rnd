package com.ba.gas.monkey.resources.customer.app;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.customer.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class CustomerInfoResource {

    private final CustomerService customerService;

    @GetMapping("app-user-info/{mobileNo}")
    public ResponseEntity<?> appUserInfo(@PathVariable String mobileNo) {
        return new ResponseEntity<>(customerService.getByPhoneNo(mobileNo), HttpStatus.OK);
    }

}

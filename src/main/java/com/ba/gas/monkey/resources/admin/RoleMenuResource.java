package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.appmenu.AssignMenuCreateBean;
import com.ba.gas.monkey.services.RoleMenuService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@AdminV1API
@AllArgsConstructor
public class RoleMenuResource {

    private final RoleMenuService roleMenuService;

    @GetMapping("assign-role-menu/{roleId}")
    public ResponseEntity<?> getAssignMenu(@PathVariable("roleId") String roleId) {
        return new ResponseEntity<>(roleMenuService.getAssignMenuList(roleId), HttpStatus.OK);
    }

    @PatchMapping("assign-role-menu/{roleId}")
    public ResponseEntity<?> saveAssignMenu(@PathVariable("roleId") String roleId, @RequestBody @Valid List<AssignMenuCreateBean> beans) {
        return new ResponseEntity<>(roleMenuService.saveAssignMenu(roleId, beans), HttpStatus.OK);
    }

}

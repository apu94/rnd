package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.reportissue.CreateReportIssueRequestBean;
import com.ba.gas.monkey.dtos.reportissue.ReportIssueBean;
import com.ba.gas.monkey.dtos.reportissue.ReportIssueUpdateBean;
import com.ba.gas.monkey.services.ReportIssueService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ReportIssueResource {

    private final ReportIssueService service;

    @GetMapping("support-ticket")
    public ResponseEntity<?> getReportIssues(Pageable pageable) {
        return new ResponseEntity<>(service.getReportIssues(pageable), HttpStatus.OK);
    }

    @GetMapping("support-ticket/{id}")
    public ResponseEntity<?> getReportIssueById(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @PutMapping("support-ticket/{id}")
    public ResponseEntity<?> updateReportIssue(@PathVariable("id") String id, @RequestBody @Valid ReportIssueUpdateBean issueBean) {
        return new ResponseEntity<>(service.updateReportIssue(id, issueBean), HttpStatus.OK);
    }

    @PostMapping("support-ticket")
    public ResponseEntity<?> createSupportTicket(@RequestBody @Valid CreateReportIssueRequestBean bean) {
        return new ResponseEntity<>(service.createSupportTicket(bean), HttpStatus.OK);
    }

}

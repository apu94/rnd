package com.ba.gas.monkey.resources.partner;

import com.ba.gas.monkey.services.PartnerDashboardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;

@PartnerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('PARTNER')")
public class PartnerDashboardResource {

    private final PartnerDashboardService partnerDashboardService;

    @GetMapping("dashboard")
    public ResponseEntity<?> dashboardData() {
        return new ResponseEntity<>(partnerDashboardService.dashboardData(), HttpStatus.OK);
    }

    @GetMapping("home-data")
    public ResponseEntity<?> homeData() {
        return new ResponseEntity<>(partnerDashboardService.homeDataAverageTimeAndOrderDelivery(), HttpStatus.OK);
    }
}

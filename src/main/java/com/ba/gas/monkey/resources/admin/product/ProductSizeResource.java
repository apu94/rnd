package com.ba.gas.monkey.resources.admin.product;

import com.ba.gas.monkey.dtos.product.ProductSizeBean;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.product.ProductSizeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ProductSizeResource {

    private final ProductSizeService service;

    @GetMapping("product-size")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getProductSizeList(pageable), HttpStatus.OK);
    }

    @GetMapping("product-size/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("product-size/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id){
        return new ResponseEntity<>(service.getByOid(id),HttpStatus.OK);
    }

    @PostMapping("product-size")
    public ResponseEntity<?> CreateProductSize(@RequestBody @Valid ProductSizeBean bean){
         return new ResponseEntity<>(service.createProductSize(bean),HttpStatus.OK);
    }
    @DeleteMapping("product-size/{id}")
    public ResponseEntity<?> deleteProductSize(@PathVariable("id") String id){
        return new ResponseEntity<>(service.deleteProductSize(id), HttpStatus.OK);
    }

    @PutMapping("product-size/{id}")
    public ResponseEntity<?> updateProductSize(@PathVariable("id") String id, @RequestBody @Valid ProductSizeBean bean){
        return new ResponseEntity<>(service.updateProductSize(id,bean), HttpStatus.OK);
    }
}

package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.services.partner.PartnerReviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@AdminV1API
@AllArgsConstructor
public class RatingResource {

    private PartnerReviewService partnerReviewService;

    @GetMapping("rating-details/{partnerId}")
    public ResponseEntity<?> getRatingDetails(@PathVariable("partnerId") String partnerId) {
        return new ResponseEntity<>(partnerReviewService.getRatingDetails(partnerId), HttpStatus.OK);
    }
}

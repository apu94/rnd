package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.services.GroupInfoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.constraints.Min;


@AdminV1API
@AllArgsConstructor
public class GroupInfoResource {

    private GroupInfoService service;

    @GetMapping("group-info")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getList(pageable), HttpStatus.OK);
    }

    @GetMapping("group-info/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("group-info/{id}")
    public ResponseEntity<?> getByOid(@PathVariable("id") @Min(36) String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }
}

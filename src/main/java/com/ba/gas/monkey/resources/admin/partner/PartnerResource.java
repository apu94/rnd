package com.ba.gas.monkey.resources.admin.partner;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.dealer.DealerCreateBean;
import com.ba.gas.monkey.dtos.partner.PartnerCreateBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.dealer.DealerDetailsService;
import com.ba.gas.monkey.services.partner.PartnerDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class PartnerResource {

    private final PartnerDetailsService service;

    @GetMapping("partner")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getPartnerListBeans(pageable), HttpStatus.OK);
    }

    @GetMapping("partner/{id}")
    public ResponseEntity<?> getPartnerDetails(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getPartnerDetails(id), HttpStatus.OK);
    }

    @PostMapping("partner")
    public ResponseEntity<?> createPartner(@RequestBody @Valid PartnerCreateBean bean) {
        return new ResponseEntity<>(service.createPartner(bean), HttpStatus.OK);
    }

    @PutMapping("partner/approved/{id}")
    public ResponseEntity<?> approvedPartner(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.approvedPartner(id), HttpStatus.OK);
    }

    @GetMapping("partner/dropdown-list/{clusterId}")
    public ResponseEntity<?> dropdownList(@PathVariable("clusterId") String clusterId) {
        return new ResponseEntity<>(service.dropdownList(clusterId), HttpStatus.OK);
    }

    @GetMapping("partner/payment/{id}")
    public ResponseEntity<?> partnerPaymentInfo(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.partnerPaymentInfo(id), HttpStatus.OK);
    }

    @PutMapping("partner/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

    @PutMapping("partner/{id}")
    public ResponseEntity<?> updatePartner(@PathVariable("id") String id, @RequestBody @Valid PartnerCreateBean bean) {
        return new ResponseEntity<>(service.updatePartner(id, bean), HttpStatus.OK);
    }
}

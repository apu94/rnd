package com.ba.gas.monkey.resources.admin.customer;

import com.ba.gas.monkey.dtos.DistanceBean;
import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class CustomerResource {

    private final CustomerService service;

    @GetMapping("customer")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getCustomerListBeans(pageable), HttpStatus.OK);
    }

    @GetMapping("customer/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("customer/{id}")
    public ResponseEntity<?> getList(@PathVariable String id) {
        return new ResponseEntity<>(service.getCustomerInfoById(id), HttpStatus.OK);
    }

    @PostMapping("customer")
    public ResponseEntity<?> createCustomer(@RequestBody @Valid CustomerBean customerBean) {
        return new ResponseEntity<>(service.createCustomer(customerBean), HttpStatus.OK);
    }

    @PostMapping("customer-distance")
    public ResponseEntity<?> nearestPartner(@RequestBody @Valid DistanceBean distanceBean, Pageable pageable) {
        return new ResponseEntity<>(service.getAllNearestDistanceList(distanceBean, pageable), HttpStatus.OK);
    }

    @PutMapping("customer/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

    @PostMapping("customer/{id}")
    public ResponseEntity<?> createCustomer(@PathVariable("id") String id, @RequestBody @Valid CustomerBean customerBean) {
        return new ResponseEntity<>(service.updateCustomer(id,customerBean), HttpStatus.OK);
    }

}

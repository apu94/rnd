package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.issuetype.IssueTypeBean;
import com.ba.gas.monkey.services.IssueTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class IssueTypeResource {

    private final IssueTypeService service;

    @GetMapping("issue-type")
    public ResponseEntity<?> getIssueTypeList(Pageable pageable) {
        return new ResponseEntity<>(service.getIssueTypeList(pageable), HttpStatus.OK);
    }

    @GetMapping("issue-type/{id}")
    public ResponseEntity<?> getByOid(@PathVariable String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @GetMapping("issue-type/dropdown-list")
    public ResponseEntity<?> getIssueTypeDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("issue-type")
    public ResponseEntity<?> create(@RequestBody @Valid IssueTypeBean typeBean) {
        return new ResponseEntity<>(service.save(typeBean), HttpStatus.OK);
    }

    @PutMapping("issue-type/{id}")
    public ResponseEntity<?> updateType(@PathVariable String id, @RequestBody @Valid IssueTypeBean typeBean) {
        return new ResponseEntity<>(service.updateType(id, typeBean), HttpStatus.OK);
    }

}

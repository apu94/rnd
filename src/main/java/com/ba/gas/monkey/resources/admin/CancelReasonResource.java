package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.cancelreason.CancelReasonBean;
import com.ba.gas.monkey.services.CancelReasonService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class CancelReasonResource {

    private final CancelReasonService service;

    @GetMapping("cancel-reason")
    public ResponseEntity<?> getCancelReasonList(Pageable pageable) {
        return new ResponseEntity<>(service.getCancelReasonList(pageable), HttpStatus.OK);
    }

    @GetMapping("cancel-reason/{id}")
    public ResponseEntity<?> getCancelReasonList(@PathVariable String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @GetMapping("cancel-reason/dropdown-list")
    public ResponseEntity<?> getCancelReasonDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("cancel-reason")
    public ResponseEntity<?> create(@RequestBody @Valid CancelReasonBean reasonBean) {
        return new ResponseEntity<>(service.save(reasonBean), HttpStatus.OK);
    }

    @PutMapping("cancel-reason/{id}")
    public ResponseEntity<?> updateReason(@PathVariable String id, @RequestBody @Valid CancelReasonBean reasonBean) {
        return new ResponseEntity<>(service.updateReason(id,reasonBean), HttpStatus.OK);
    }
}

package com.ba.gas.monkey.resources.customer.coupon;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@CustomerV1API
@AllArgsConstructor
public class CustomerCouponResource {
    private final CouponService service;

    @GetMapping("coupon")
    public ResponseEntity<?> getCouponDetail(@RequestParam String code) {
        return new ResponseEntity<>(service.getCouponDetailsBean(code), HttpStatus.OK);
    }
}

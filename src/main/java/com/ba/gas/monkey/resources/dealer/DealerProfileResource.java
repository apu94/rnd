package com.ba.gas.monkey.resources.dealer;

import com.ba.gas.monkey.dtos.ProfileUpdateBean;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.dealer.DealerDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@DealerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('DEALER')")
public class DealerProfileResource {

    private final DealerDetailsService detailsService;

    @GetMapping("profile")
    public ResponseEntity<?> profile() {
        return new ResponseEntity<>(detailsService.profile(), HttpStatus.OK);
    }

    @PostMapping("profile")
    public ResponseEntity<?> profileUpdate(@RequestBody @Valid ProfileUpdateBean bean) {
        return new ResponseEntity<>(detailsService.profileUpdate(bean), HttpStatus.OK);
    }

    @DeleteMapping("profile")
    public ResponseEntity<?> deletePartner() {
        return new ResponseEntity<>(detailsService.deletePartner(), HttpStatus.OK);
    }
}

package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.cart.AddCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.ChangeCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.DeleteCartBeanRequest;
import com.ba.gas.monkey.services.cart.CartDetailService;
import com.ba.gas.monkey.services.cart.CartService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class CartResource {

    private final CartService cartService;
    private final CartDetailService cartDetailService;

    @PostMapping("add-to-cart")
    public ResponseEntity<?> addToCart(@RequestBody @Valid AddCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.addToCart(bean), HttpStatus.OK);
    }

    @PostMapping("delete-from-cart")
    public ResponseEntity<?> deleteFromCart(@RequestBody @Valid DeleteCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.deleteFromCart(bean), HttpStatus.OK);
    }

    @GetMapping("get-cart/{id}")
    public ResponseEntity<?> getCart(@PathVariable String id) {
        return new ResponseEntity<>(cartService.getCart(id), HttpStatus.OK);
    }

    @GetMapping("repeat-order-get-cart/{id}")
    public ResponseEntity<?> repeatOrderGetCart(@PathVariable String id) {
        return new ResponseEntity<>(cartService.repeatOrderGetCart(id), HttpStatus.OK);
    }

    @PostMapping("change-cart-request")
    public ResponseEntity<?> changeCartRequest(@RequestBody @Valid ChangeCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.changeCart(bean), HttpStatus.OK);
    }

    @PostMapping("coupon-add-remove-request")
    public ResponseEntity<?> couponAddRemoveRequest(@RequestBody @Valid ChangeCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.couponAddRemove(bean), HttpStatus.OK);
    }
}

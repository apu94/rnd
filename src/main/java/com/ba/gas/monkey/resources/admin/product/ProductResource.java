package com.ba.gas.monkey.resources.admin.product;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.product.ProductBean;
import com.ba.gas.monkey.dtos.product.ProductCreateBean;
import com.ba.gas.monkey.dtos.product.ProductEditBean;
import com.ba.gas.monkey.paylod.request.ProductRequestBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.product.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ProductResource {
    private final ProductService service;

    @GetMapping("product")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getProductList(pageable), HttpStatus.OK);
    }

    @GetMapping("product/{id}")
    public ResponseEntity<?> findProduct(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getProductById(id), HttpStatus.OK);
    }

    @PostMapping("product")
    public ResponseEntity<?> createProduct(@RequestBody @Valid ProductCreateBean productCreateBean) {
        return new ResponseEntity<>(service.createProduct(productCreateBean), HttpStatus.OK);
    }

    @PutMapping("product/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") String id, @RequestBody @Valid ProductEditBean productEditBean) {
        return new ResponseEntity<>(service.updateProduct(id, productEditBean), HttpStatus.OK);
    }

    @PutMapping("product/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }
}

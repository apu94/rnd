package com.ba.gas.monkey.resources.admin.order;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.order.ForwardOrderBean;
import com.ba.gas.monkey.dtos.order.OrderStatusBean;
import com.ba.gas.monkey.dtos.order.OrderStatusUpdateBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.order.OrderProductService;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class AdminOrderResource {

    private final OrderService service;

    @GetMapping("order-list")
    public ResponseEntity<?> getOrderList(Pageable pageable) {
        return new ResponseEntity<>(service.getOrderList(pageable), HttpStatus.OK);
    }

    @GetMapping("get-order-by-id/{id}")
    public ResponseEntity<?> getOrderById(@PathVariable String id) {
        return new ResponseEntity<>(service.getAdminOrder(id), HttpStatus.OK);
    }

    @PostMapping("forward-order")
    public ResponseEntity<?> updateOrderStatus(@RequestBody @Valid ForwardOrderBean bean) {
        return new ResponseEntity<>(service.forwardOrder(bean), HttpStatus.OK);
    }

    @PutMapping("order/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

    @PutMapping("order/update-order-status/{id}")
    public ResponseEntity<?> updateOrderStatus(@PathVariable("id") String id, @RequestBody @Valid OrderStatusBean statusBean) {
        return new ResponseEntity<>(service.updateOrderStatus(id, statusBean), HttpStatus.OK);
    }

}

package com.ba.gas.monkey.resources.admin.product;

import com.ba.gas.monkey.dtos.product.ProductValveSizeBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.product.ProductValveSizeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ProductValveSizeResource {

    private final ProductValveSizeService service;

    @GetMapping("product-valve-size")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getProductValveSizeList(pageable), HttpStatus.OK);
    }

    @GetMapping("product-valve-size/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }
    @GetMapping("product-valve-size/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id){
        return new ResponseEntity<>(service.getByOid(id),HttpStatus.OK);
    }

    @PostMapping("product-valve-size")
    public ResponseEntity<?> createValveSize(@RequestBody @Valid ProductValveSizeBean bean){
        return new ResponseEntity<>(service.createValveSize(bean), HttpStatus.OK);
    }

    @DeleteMapping("product-valve-size/{id}")
    public ResponseEntity<?> deleteValveSize(@PathVariable("id") String id){
        return new ResponseEntity<>(service.deleteValveSize(id), HttpStatus.OK);
    }

    @PutMapping("product-valve-size/{id}")
    public ResponseEntity<?> updateValveSize(@PathVariable("id") String id, @RequestBody @Valid ProductValveSizeBean bean){
        return new ResponseEntity<>(service.updateValveSize(id,bean), HttpStatus.OK);
    }


}

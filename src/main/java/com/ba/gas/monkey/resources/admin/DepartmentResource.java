package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.department.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class DepartmentResource {

    private final DepartmentService service;

    @GetMapping("department/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("department")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getDepartmentList(pageable), HttpStatus.OK);
    }

    @GetMapping("department/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @PostMapping("department")
    public ResponseEntity<?> createDepartment(@RequestBody @Valid DepartmentBean bean) {
        return new ResponseEntity<>(service.createDepartment(bean), HttpStatus.OK);
    }

    @PutMapping("department/{id}")
    public ResponseEntity<?> updateDepartment(@PathVariable("id") String id, @RequestBody @Valid DepartmentBean bean) {
        return new ResponseEntity<>(service.updateDepartment(id, bean), HttpStatus.OK);
    }

    @PutMapping("department/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

}

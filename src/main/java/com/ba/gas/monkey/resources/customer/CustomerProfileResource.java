package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.ProfileUpdateBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.services.customer.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class CustomerProfileResource {

    private final CustomerService customerService;

    @GetMapping("profile")
    public ResponseEntity<?> profile() {
        return new ResponseEntity<>(customerService.profile(), HttpStatus.OK);
    }

    @PostMapping("profile")
    public ResponseEntity<?> profileUpdate(@RequestBody @Valid ProfileUpdateBean bean) {
        return new ResponseEntity<>(customerService.profileUpdate(bean), HttpStatus.OK);
    }

    @DeleteMapping("{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("customerId") String customerId) {
        return new ResponseEntity<>(customerService.deleteCustomer(customerId), HttpStatus.OK);
    }
}

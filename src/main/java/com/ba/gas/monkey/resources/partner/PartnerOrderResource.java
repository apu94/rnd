package com.ba.gas.monkey.resources.partner;

import com.ba.gas.monkey.services.CancelReasonService;
import com.ba.gas.monkey.services.order.OrderProductService;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@PartnerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('PARTNER')")
public class PartnerOrderResource {

    private final OrderService orderService;
    private final OrderProductService orderProductService;
    private final CancelReasonService cancelReasonService;


    @GetMapping("requested-order-list/{id}")
    public ResponseEntity<?> requestedOrderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getRequestedOrderList(id), HttpStatus.OK);
    }

    @GetMapping("past-order-list/{id}")
    public ResponseEntity<?> pastOrderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getPastOrderList(id), HttpStatus.OK);
    }

    @GetMapping("running-order-list/{id}")
    public ResponseEntity<?> runningOrderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getRunningOrderList(id), HttpStatus.OK);
    }

    @GetMapping("past-order-list-current-month/{id}")
    public ResponseEntity<?> partnerPastOrderListCurrentMonth(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getPartnerPastOrderListCurrentMonth(id), HttpStatus.OK);
    }

    @GetMapping("transaction-summary/{id}")
    public ResponseEntity<?> partnerTransactionSummaryMonth(@PathVariable String id, @RequestParam String date ) {
        return new ResponseEntity<>(orderService.getPartnerTransactionSummaryMonth(id, date), HttpStatus.OK);
    }
}

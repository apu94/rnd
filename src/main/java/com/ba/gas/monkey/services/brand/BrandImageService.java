package com.ba.gas.monkey.services.brand;


import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.brand.BrandImageBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.BrandImage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service("BrandImageService")
public class BrandImageService extends BaseService<BrandImage, BrandImageBean> {

    private final BrandService brandService;

    public BrandImageService(BaseRepository<BrandImage> repository, ModelMapper modelMapper, BrandService brandService) {
        super(repository, modelMapper);
        this.brandService = brandService;
    }

    public BrandImageBean createBrandImage(BrandImageBean data) {
        BrandImage brandImage = convertForCreate(data);
        brandService.getRepository().findById(data.getBrandId()).ifPresent(brandImage::setBrand);
        return super.create(brandImage);
    }

    public BrandImageBean updateBrandImage(String id, BrandImageBean bean) {
        BrandImage brandImage = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No brand-image found by id:" + id));
        BeanUtils.copyProperties(bean, brandImage, AppConstant.IGNORE_PROPERTIES);
        return update(id, brandImage);
    }

    public String deleteByOid(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }



}

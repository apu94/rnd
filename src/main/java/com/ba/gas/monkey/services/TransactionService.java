package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.PaymentBean;
import com.ba.gas.monkey.dtos.transaction.TransactionBean;
import com.ba.gas.monkey.dtos.transaction.TransactionCreateBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.Transaction;
import com.ba.gas.monkey.repositories.TransactionRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.partner.PartnerDetailsService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("transactionService")
public class TransactionService extends BaseService<Transaction, TransactionBean> {

    private final CustomerService customerService;
    private final PartnerDetailsService partnerDetailsService;

    public TransactionService(BaseRepository<Transaction> repository, ModelMapper modelMapper,
                              CustomerService customerService,PartnerDetailsService partnerDetailsService) {
        super(repository, modelMapper);
        this.customerService = customerService;
        this.partnerDetailsService = partnerDetailsService;
    }

    public Object payment(TransactionCreateBean transactionCreateBean) {
        TransactionBean transactionBean = getModelMapper().map(transactionCreateBean, TransactionBean.class);
        Customer partner = customerService.getRepository().getById(transactionCreateBean.getCustomerId());
        Transaction transaction = convertForCreate(transactionBean);
        transaction.setPartner(partner);
        transaction.setStatus(Boolean.TRUE);
        transaction.setDebit(transactionCreateBean.getAmount());
        transaction.setCredit(0.0);
        transaction.setNote(transactionBean.getNote());
        transaction.setBalance(transactionCreateBean.getAmount());
        List<Transaction> list = ((TransactionRepository) getRepository()).findByPartner(partner, PageRequest.of(0, 1));
        if (!list.isEmpty()) {
            Transaction existingTransaction = list.get(0);
            transaction.setBalance(existingTransaction.getBalance()-transactionCreateBean.getAmount());
        }
        create(transaction);
        partnerDetailsService.updatePayableAmount(transactionCreateBean);
        return PaymentBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public void addAmount(TransactionCreateBean transactionCreateBean) {
        TransactionBean transactionBean = getModelMapper().map(transactionCreateBean, TransactionBean.class);
        Customer partner = customerService.getRepository().getById(transactionCreateBean.getCustomerId());
        Transaction transaction = convertForCreate(transactionBean);
        transaction.setPartner(partner);
        transaction.setStatus(Boolean.TRUE);
        transaction.setDebit(0.0);
        transaction.setCredit(transactionCreateBean.getAmount());
        transaction.setNote(transactionBean.getNote());
        transaction.setBalance(transactionCreateBean.getAmount());
        List<Transaction> list = ((TransactionRepository) getRepository()).findByPartner(partner, PageRequest.of(0, 1));
        if (!list.isEmpty()) {
            Transaction existingTransaction = list.get(0);
            transaction.setBalance(transactionCreateBean.getAmount() + existingTransaction.getBalance());
        }
        create(transaction);
        PaymentBean.builder().msg(AppConstant.SUCCESS).build();
    }
}

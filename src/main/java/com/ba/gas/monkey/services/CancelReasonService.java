package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.cancelreason.CancelReasonBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.cancelreason.CancelReasonListBean;
import com.ba.gas.monkey.models.CancelReason;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("cancelReasonService")
public class CancelReasonService extends BaseService<CancelReason, CancelReasonBean> {

    public CancelReasonService(BaseRepository<CancelReason> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<CancelReason> getCustomerCancelReasonList() {
        return getRepository().findAll();
    }

    public List<DropdownDTO> getDropdownList() {
        return getRepository().findAll().stream().filter(CancelReason::getStatus)
                .map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(CancelReason cancelReason) {
        DropdownDTO dto = getModelMapper().map(cancelReason, DropdownDTO.class);
        dto.setValue(cancelReason.getTitle());
        return dto;
    }

    public CancelReasonBean save(CancelReasonBean reasonBean) {
        CancelReason cancelReason = convertForCreate(reasonBean);
        return create(cancelReason);
    }

    public CancelReasonBean updateReason(String id, CancelReasonBean reasonBean) {
        CancelReason cancelReason = getRepository().getById(id);
        BeanUtils.copyProperties(reasonBean, cancelReason, AppConstant.IGNORE_PROPERTIES);
        return update(id, cancelReason);
    }

    public Page<CancelReasonListBean> getCancelReasonList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title"));
        Page<CancelReason> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private CancelReasonListBean getListBean(CancelReason cancelReason) {
        CancelReasonListBean bean = getModelMapper().map(cancelReason, CancelReasonListBean.class);
        bean.setStatus(cancelReason.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}

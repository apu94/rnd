package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.config.RabbitConfig;
import com.ba.gas.monkey.dtos.firebase.TransactionMaster;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AMQPMessagePublisher {
    private final RabbitTemplate publishAMQPMessage;
    private final ObjectMapper objectMapper;

    public AMQPMessagePublisher(RabbitTemplate publishAMQPMessage, ObjectMapper objectMapper) {
        this.publishAMQPMessage = publishAMQPMessage;
        this.objectMapper = objectMapper;
    }

    public void publishAMQPMessage(String routingKey, TransactionMaster message) {
        try {
            String s = objectMapper.writeValueAsString(message);
            publishAMQPMessage.convertAndSend(RabbitConfig.AMQP_EXCHANGE,routingKey, s);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
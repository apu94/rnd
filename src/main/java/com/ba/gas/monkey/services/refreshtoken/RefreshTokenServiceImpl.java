package com.ba.gas.monkey.services.refreshtoken;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.refreshtoken.RefreshTokenBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.RefreshToken;
import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.repositories.RefreshTokenRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.user.UserInfoService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Service("refreshTokenService")
class RefreshTokenServiceImpl implements RefreshTokenService {

    private final RefreshTokenRepository repository;
    private final UserInfoService userInfoService;
    private final CustomerService customerService;
    private final ModelMapper modelMapper;

    @Override
    public RefreshTokenBean createRefreshToken(String userId) {
        UserInfo userInfo = userInfoService.getById(userId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No user found by id: " + userId));
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken.setUserId(userInfo.getId());
        refreshToken.setModule(AppConstant.USER_TYPE_ADMIN);
        refreshToken.setExpiryDate(Instant.now().plusMillis(AppConstant.REFRESH_TOKEN_EXPIRE_IN));
        RefreshToken createdRefreshToken = repository.save(refreshToken);
        return getDto(createdRefreshToken);
    }

    @Override
    public Optional<RefreshTokenBean> getByToken(String token) {
        return repository.findByToken(token).map(this::getDto);
    }

    @Override
    public RefreshTokenBean verifyExpiration(RefreshTokenBean token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            repository.delete(repository.getById(token.getId()));
            throw new ServiceExceptionHolder.RefreshTokenException("Your refresh token was expired. Please make a new login request");
        }
        return token;
    }

    private RefreshTokenBean getDto(RefreshToken refreshToken) {
        return modelMapper.map(refreshToken, RefreshTokenBean.class);
    }

    @Override
    public RefreshTokenBean createRefreshTokenForApp(String customerId) {
        Customer customer = customerService.getRepository().getById(customerId);
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken.setUserId(customer.getId());
        refreshToken.setModule(AppConstant.APP_MODULE);
        refreshToken.setExpiryDate(Instant.now().plusMillis(AppConstant.REFRESH_TOKEN_EXPIRE_IN));
        RefreshToken createdRefreshToken = repository.save(refreshToken);
        return getDto(createdRefreshToken);
    }
}

package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.coupon.CouponBean;
import com.ba.gas.monkey.dtos.coupon.CouponListBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Coupon;
import com.ba.gas.monkey.repositories.CouponRepository;
import com.ba.gas.monkey.utility.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("couponService")
public class CouponService extends BaseService<Coupon, CouponBean> {

    public CouponService(BaseRepository<Coupon> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public CouponBean createCoupon(CouponBean couponBean) {
        return super.create(convertForCreate(couponBean));
    }


    public CouponBean updateCoupon(String id, CouponBean bean) {
        Coupon coupon = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by id:" + id));
        BeanUtils.copyProperties(bean, coupon, AppConstant.IGNORE_PROPERTIES);
        return update(id, coupon);
    }

    public String deleteByOid(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }

    public CouponBean getCouponDetailsBean(String code) {
        Coupon coupon = ((CouponRepository) getRepository()).findByCouponCode(code).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by code:" + code));
        if (Objects.equals(coupon.getUsageLimit(), coupon.getCouponUsed())) {
            throw new ServiceExceptionHolder.IdNotFoundInDBException("Coupon already used! code:" + code);
        }
        if(!DateUtils.isDateInBetweenIncludingEndPoints(coupon.getStartDate(), coupon.getEndDate(), LocalDateTime.now())){
            throw new ServiceExceptionHolder.IdNotFoundInDBException("Coupon already expired! code:" + code);
        }
        return getModelMapper().map(coupon, CouponBean.class);
    }

    public Page<CouponListBean> getCouponList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("status"),Sort.Order.desc("endDate")));
        Page<Coupon> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getCouponListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private CouponListBean getCouponListBean(Coupon coupon) {
        CouponListBean bean = getModelMapper().map(coupon, CouponListBean.class);
        bean.setStatus(coupon.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}

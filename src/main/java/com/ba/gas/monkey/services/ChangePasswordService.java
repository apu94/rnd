package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.changepassword.ChangePasswordBean;
import com.ba.gas.monkey.dtos.changepassword.ChangePasswordResponseBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Objects;

@AllArgsConstructor
@Service("changePasswordService")

public class ChangePasswordService {

    private final CustomerService customerService;
    private final PasswordEncoder passwordEncoder;

    public ChangePasswordResponseBean changePassword(ChangePasswordBean passwordBean) {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        boolean valid = passwordEncoder.matches(passwordBean.getOldPassword(), customer.getPassword());
        if (!valid) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, "please check old password");
        }
        String password = passwordBean.getNewPassword();
        String confirmPassword = passwordBean.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, "please check password");
        }
        customer.setPassword(passwordEncoder.encode(password));
        customer.setDateModified(Instant.now());
        customerService.getRepository().save(customer);
        return ChangePasswordResponseBean.builder().success(Boolean.TRUE).build();
    }
}

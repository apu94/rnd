package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DeviceTokenBean;
import com.ba.gas.monkey.dtos.pushnotification.PushNotificationBean;
import com.ba.gas.monkey.dtos.pushnotification.PushNotificationResponseBean;
import com.ba.gas.monkey.dtos.notification.NotificationBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.repositories.NotificationDetailsRepository;
import com.ba.gas.monkey.repositories.NotificationRepository;
import com.ba.gas.monkey.services.DeviceTokenService;
import com.ba.gas.monkey.services.TemplateService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("notificationService")
public class NotificationService extends BaseService<Notification, NotificationBean> {

    private final DeviceTokenService deviceTokenService;
    private final NotificationRepository notificationRepository;
    private final CustomerService customerService;
    private final NotificationTypeService notificationTypeService;
    private final TemplateService templateService;
    private final NotificationDetailsRepository notificationDetailsRepository;

    public NotificationService(BaseRepository<Notification> repository, ModelMapper modelMapper,
                               DeviceTokenService deviceTokenService,
                               NotificationRepository notificationRepository,
                               CustomerService customerService,
                               NotificationTypeService notificationTypeService,
                               TemplateService templateService,
                               NotificationDetailsRepository notificationDetailsRepository) {
        super(repository, modelMapper);
        this.deviceTokenService = deviceTokenService;
        this.notificationRepository = notificationRepository;
        this.customerService = customerService;
        this.notificationTypeService = notificationTypeService;
        this.templateService = templateService;
        this.notificationDetailsRepository = notificationDetailsRepository;
    }

    public List<Notification> getNotificationList(String id, @NotBlank String deviceType) {
        List<DeviceToken> deviceTokens = deviceTokenService.getDeviceTokenByUserIdAndDevice(id, deviceType);
        if (deviceTokens.isEmpty()) {
            return new ArrayList<>();
        }
        PageRequest pageRequest = PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<Notification> notificationList = notificationRepository.findByReceiverId(id, pageRequest);
//        return notificationList.stream().filter(notification -> notification.getDeviceType().equalsIgnoreCase(deviceType) && notification.getNotificationStatus() == 1).collect(Collectors.toList());
        return notificationList.stream().filter(notification -> notification.getNotificationStatus() == 1).collect(Collectors.toList());
    }

    public Notification customerNotificationSeen(String id) {
        Notification notification = getRepository().getById(id);
        notification.setSeen(true);
        notification.setStatus(Boolean.TRUE);
        return notificationRepository.save(notification);
    }

    public Object getNotifications() {
        String userId = AuthenticationUtils.getTokenUserId();
        List<DeviceTokenBean> deviceTokenBeanList = deviceTokenService.getDeviceTokenByUserId(userId);
        if (deviceTokenBeanList.isEmpty()) {
            return null;
        }
        DeviceTokenBean deviceTokenBean = deviceTokenBeanList.stream().filter(dt -> dt.getDeviceType().equalsIgnoreCase("WEB")).collect(Collectors.toList()).get(0);
        PageRequest pageRequest = PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<Notification> notificationList = notificationRepository.findByReceiverId(userId, pageRequest);
        return notificationList.stream().filter(notification -> notification.getDeviceType().equalsIgnoreCase(deviceTokenBean.getDeviceType()) && notification.getNotificationStatus() == 1).collect(Collectors.toList());
    }

    public void createPushNotification(PushNotificationBean notificationBean) {
        List<Customer> customers = ((CustomerRepository) customerService.getRepository()).getByTypeId(notificationBean.getTypeId());
        process(notificationBean, customers);
        PushNotificationResponseBean.builder().success(Boolean.TRUE).msg(AppConstant.SUCCESS).build();
    }

    @Async
    private void process(PushNotificationBean pushNotificationBean, List<Customer> customers) {
        Optional<NotificationType> optional = notificationTypeService.getByType(AppConstant.NOTIFICATION_TYPE_PROMOTIONAL);
        if (optional.isEmpty()) {
            return;
        }
        List<String> customerIds = customers.stream().map(Customer::getId).collect(Collectors.toUnmodifiableList());
        List<DeviceToken> deviceTokens = deviceTokenService.getDeviceTokenByUserIds(customerIds);
        Template template = templateService.getRepository().getById(pushNotificationBean.getTemplateId());
        deviceTokens.forEach(l -> {
            saveNotification(template, l, optional.get());
        });
    }

    public void saveNotification(Template template, DeviceToken deviceToken, NotificationType notificationType) {
        Notification notification = new Notification();
        notification.setNotificationStatus(-1);
        notification.setNotificationType(notificationType.getId());
        notification.setMediaType(AppConstant.PUSH);
        notification.setNoOfTry(1);
        notification.setReceiverId(deviceToken.getUserId());
        notification.setModule(AppConstant.APP_MODULE);
        notification.setDeviceType(deviceToken.getDeviceType());
        notification.setDeviceToken(deviceToken.getToken());
        notification.setUpdtId(AuthenticationUtils.getTokenUserId());
        notification.setDateCreated(Instant.now());
        notification.setDateModified(Instant.now());
        notification.setStatus(Boolean.TRUE);
        notification.setStatus(Boolean.TRUE);
        notification.setSeen(Boolean.FALSE);
        notification.setTemplate(template);
        Notification createdNotification = getRepository().save(notification);
        NotificationDetails notificationDetails = new NotificationDetails();
        notificationDetails.setNotification(createdNotification);
        notificationDetails.setSubject(template.getTitle());
        notificationDetails.setContent(template.getDescription());
        notificationDetails.setUpdtId(AuthenticationUtils.getTokenUserId());
        notificationDetails.setDateCreated(Instant.now());
        notificationDetails.setDateModified(Instant.now());
        notificationDetails.setStatus(Boolean.TRUE);
        notificationDetailsRepository.save(notificationDetails);
    }

    public List<Notification> getUnSendNotificationList() {
        return notificationRepository.findFirst15ByNotificationStatus(-1, Sort.by(Sort.Direction.DESC, "dateCreated"));
    }
}

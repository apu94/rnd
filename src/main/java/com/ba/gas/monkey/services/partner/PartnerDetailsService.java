package com.ba.gas.monkey.services.partner;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.mobilebanking.MobileBankingInfoBean;
import com.ba.gas.monkey.dtos.partner.PartnerCreateBean;
import com.ba.gas.monkey.dtos.partner.PartnerDetailsBean;
import com.ba.gas.monkey.dtos.partner.PartnerDetailsListBean;
import com.ba.gas.monkey.dtos.thana.ThanaBean;
import com.ba.gas.monkey.dtos.transaction.TransactionCreateBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.*;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.MobileBankingInfoService;
import com.ba.gas.monkey.services.ServiceChargeService;
import com.ba.gas.monkey.services.customer.CustomerAttachmentService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import com.ba.gas.monkey.utility.PasswordUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("partnerDetailsService")
public class PartnerDetailsService extends BaseService<PartnerDetails, PartnerDetailsBean> {

    private final UserInfoService userInfoService;
    private final MobileBankingInfoService mobileBankingInfoService;
    private final CustomerService customerService;
    private final CustomerTypeService customerTypeService;
    private final ClusterService clusterService;
    private final PasswordUtils passwordUtils;
    private final CustomerAttachmentService customerAttachmentService;

    private final PartnerReviewRepository partnerReviewRepository;
    private final TransactionRepository transactionRepository;
    private final OrderRepository orderRepository;
    private final ServiceChargeService serviceChargeService;

    public PartnerDetailsService(BaseRepository<PartnerDetails> repository, ModelMapper modelMapper,
                                 UserInfoService userInfoService, MobileBankingInfoService mobileBankingInfoService,
                                 CustomerService customerService, CustomerTypeService customerTypeService,
                                 ClusterService clusterService, PasswordUtils passwordUtils,
                                 CustomerAttachmentService customerAttachmentService,
                                 PartnerReviewRepository partnerReviewRepository,
                                 TransactionRepository transactionRepository,
                                 ServiceChargeService serviceChargeService,
                                 OrderRepository orderRepository) {
        super(repository, modelMapper);
        this.userInfoService = userInfoService;
        this.customerService = customerService;
        this.customerTypeService = customerTypeService;
        this.mobileBankingInfoService = mobileBankingInfoService;
        this.clusterService = clusterService;
        this.passwordUtils = passwordUtils;
        this.customerAttachmentService = customerAttachmentService;
        this.partnerReviewRepository = partnerReviewRepository;
        this.transactionRepository = transactionRepository;
        this.orderRepository = orderRepository;
        this.serviceChargeService = serviceChargeService;
    }

    public PartnerDetails savePartnerDetails(PartnerDetailsBean partnerDetailsBean) {
        PartnerDetails partnerDetails = convertForCreate(partnerDetailsBean);
        partnerDetails.setDateCreated(Instant.now());
        partnerDetails.setDateModified(Instant.now());
        partnerDetails.setMobileBankingInfo(mobileBankingInfoService.getRepository().getById(partnerDetailsBean.getMobileBankId()));
        partnerDetails.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
        return getRepository().save(partnerDetails);
    }

    public Page<PartnerDetailsListBean> getPartnerListBeans(Pageable pageable) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("approved"), Sort.Order.desc("dateCreated")));
        Page<Customer> page = ((CustomerRepository) customerService.getRepository()).findByCustomerType(customerType, pageRequest);
        List<PartnerDetailsListBean> beanList = page.getContent().stream().map(this::getPartnerDetailsListBean)
                .sorted(Comparator.comparing(PartnerDetailsListBean::getCurrentStatus).reversed())
                .collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(beanList, page.getPageable(), page.getTotalElements());
    }

    private PartnerDetailsListBean getPartnerDetailsListBean(Customer customer) {
        System.out.println("Phone No" + customer.getPhoneNo() + " =========" + customer.getStatus());
        PartnerDetailsBean bean = getByCustomerId(customer.getId());
        bean.setCustomerInfoBean(getModelMapper().map(customer, CustomerInfoBean.class));
        PartnerDetailsListBean listBean = getModelMapper().map(bean, PartnerDetailsListBean.class);
        Optional<MobileBankingInfoBean> mobileWallet = Optional.ofNullable(bean.getMobileWallet());
        Optional<ClusterBean> workingZoneBean = Optional.ofNullable(bean.getWorkingZoneBean());
        listBean.setStatus(customer.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        listBean.setMobileBankName((mobileWallet.isPresent()) ? bean.getMobileWallet().getName() : AppConstant.EMPTY_STRING);
        listBean.setWorkingZone(workingZoneBean.isPresent() ? bean.getWorkingZoneBean().getName() : AppConstant.EMPTY_STRING);
        listBean.setApproved((bean.getCustomerInfoBean().getApproved()) ? AppConstant.APPROVED : AppConstant.NOT_APPROVED);
        listBean.setDepositBalance(bean.getTotalPayable());
        listBean.setRating(ratingPoint(customer));
        listBean.setCurrentStatus((!customer.getUserStatus().equalsIgnoreCase(AppConstant.INACTIVE)));
        return listBean;
    }

    private Double ratingPoint(Customer customer) {
        List<PartnerReview> list = partnerReviewRepository.findByPartner(customer);
        List<Double> ratings = list.stream().map(PartnerReview::getReviewRating).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        return average.orElse(0.0);
    }

    private PartnerDetailsBean getPartnerDetailsBean(Customer customer) {
        PartnerDetailsBean bean = getByCustomerId(customer.getId());
        bean.setCustomerInfoBean(getModelMapper().map(customer, CustomerInfoBean.class));
        return bean;
    }

    private PartnerDetailsBean getByCustomerId(String customerId) {
        Optional<PartnerDetails> partnerDetails = ((PartnerDetailsRepository) getRepository()).findByCustomerId(customerId);
        if (partnerDetails.isEmpty()) {
            return new PartnerDetailsBean();
        }
        PartnerDetailsBean bean = getModelMapper().map(partnerDetails.get(), PartnerDetailsBean.class);
        bean.setMobileWallet(getModelMapper().map(partnerDetails.get().getMobileBankingInfo(), MobileBankingInfoBean.class));
        bean.setWorkingZoneBean(clusterService.getByOid(partnerDetails.get().getWorkingZone()));
        return bean;
    }

    public Page<PartnerDetailsBean> getNearestPartner(DistanceBean distanceBean, List<String> partnerIds, Pageable pageable) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        distanceBean.setId(customerType.getId());
        Page<Customer> page = customerService.getNearestDistanceList(distanceBean, partnerIds, pageable);
        List<PartnerDetailsBean> detailsBeans = page.getContent().stream().map(this::getPartnerDetailsBean).collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(detailsBeans, page.getPageable(), page.getTotalElements());
    }

    public PartnerDetailsBean profile() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        PartnerDetailsBean bean = getByCustomerId(customer.getId());
        CustomerInfoBean infoBean = getModelMapper().map(customer, CustomerInfoBean.class);
        infoBean.setDistrict(getModelMapper().map(customer.getDistrict(), DistrictBean.class));
        infoBean.setThana(getModelMapper().map(customer.getThana(), ThanaBean.class));
        infoBean.setCluster(getModelMapper().map(customer.getCluster(), ClusterBean.class));
        bean.setCustomerInfoBean(infoBean);
        return bean;
    }

    public Page<Object[]> getNearestPartnerByDistance(DistanceBean distanceBean, Pageable pageable) {
        Optional<ServiceCharge> optional = serviceChargeService.getByServiceType(AppConstant.SEARCH_AREA_IN_KM);
        Double distance = (optional.isPresent()) ? optional.get().getServiceValue() : AppConstant.DEFAULT_DISTANCE_FOR_SEARCH;
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        distanceBean.setId(customerType.getId());
        distanceBean.setDistanceInKM(1.0);
        return customerService.getAllNearestDistanceList(distanceBean, pageable);
    }

    public PartnerDetailsBean getPartnerDetails(String id) {
        Customer customer = customerService.getRepository().getById(id);
        PartnerDetailsBean bean = getByCustomerId(customer.getId());
        CustomerInfoBean infoBean = getModelMapper().map(customer, CustomerInfoBean.class);
        infoBean.setDistrict(getModelMapper().map(customer.getDistrict(), DistrictBean.class));
        infoBean.setThana(getModelMapper().map(customer.getThana(), ThanaBean.class));
        infoBean.setCluster(getModelMapper().map(customer.getCluster(), ClusterBean.class));
        bean.setAttachmentBeans(customerAttachmentService.getAllAttachmentByCustomerId(id));
        bean.setCustomerInfoBean(infoBean);
        bean.setStatus(customer.getStatus());
        return bean;
    }

    public Object approvedPartner(String id) {
        Customer customer = customerService.getRepository().getById(id);
        customer.setApproved(Boolean.TRUE);
        customer.setUserStatus(AppConstant.ACTIVE.toLowerCase());
        customerService.update(id, customer);
        return getPartnerDetails(id);
    }

    public List<DropdownDTO> dropdownList(String clusterId) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        List<Customer> list = ((CustomerRepository) customerService.getRepository()).getByClusterIdAndCustomerType(clusterId, customerType);
        return list.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Customer customer) {
        DropdownDTO dto = getModelMapper().map(customer, DropdownDTO.class);
        dto.setValue(customer.getCustomerName());
        return dto;
    }

    public Object profileUpdate(ProfileUpdateBean bean) {
        CustomerBean customerBean = customerService.profileUpdate(bean);
        return getPartnerDetails(customerBean.getId());
    }

    public void updatePartnerDetails(PartnerDetailsBean details) {
        Optional<PartnerDetails> partnerDetails = ((PartnerDetailsRepository) getRepository()).findByCustomerId(details.getCustomerId());
        if (partnerDetails.isPresent()) {
            BeanUtils.copyProperties(details, partnerDetails.get(), AppConstant.IGNORE_PROPERTIES);
            getRepository().save(partnerDetails.get());
        }
    }

    public Object createPartner(PartnerCreateBean bean) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        bean.setCustomerTypeId(customerType.getId());
        String updateId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        CustomerRegistrationBean customerRegistrationBean = getModelMapper().map(bean, CustomerRegistrationBean.class);
        customerRegistrationBean.setApproved(Boolean.TRUE);
        customerRegistrationBean.setUserStatus(AppConstant.ACTIVE.toLowerCase());
        customerRegistrationBean.setUpdtId(updateId);
        customerRegistrationBean.setStatus(Boolean.TRUE);
        customerRegistrationBean.setPassword(passwordUtils.getDefaultPassword());
        CustomerInfoBean customerInfoBean = customerService.registration(customerRegistrationBean);
        PartnerDetailsBean details = getModelMapper().map(bean, PartnerDetailsBean.class);
        details.setCustomerId(customerInfoBean.getId());
        details.setStatus(Boolean.TRUE);
        savePartnerDetails(details);
        if (Objects.nonNull(bean.getAttachments())) {
            customerAttachmentService.createCustomerAttachment(customerInfoBean.getId(), bean.getAttachments());
        }
        return getPartnerDetails(customerInfoBean.getId());
    }

    public Object partnerPaymentInfo(String id) {
        PartnerDetails partnerDetails = ((PartnerDetailsRepository) getRepository()).findByCustomerId(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No partner info found"));
        return PaymentInfoBean.builder().id(partnerDetails.getCustomerId()).totalPayable(partnerDetails.getTotalPayable()).build();
    }

    public void updatePayableAmount(TransactionCreateBean transactionCreateBean) {
        PartnerDetails partnerDetails = ((PartnerDetailsRepository) getRepository()).findByCustomerId(transactionCreateBean.getCustomerId()).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No partner info found"));
        partnerDetails.setTotalPayable(partnerDetails.getTotalPayable() - transactionCreateBean.getAmount());
        getRepository().save(partnerDetails);
    }

    @Transactional
    public Object updatePartner(String id, PartnerCreateBean bean) {
        customerService.updateCustomer(id, getModelMapper().map(bean, CustomerBean.class));
        Optional<PartnerDetails> partnerDetails = ((PartnerDetailsRepository) getRepository()).findByCustomerId(id);
        if (partnerDetails.isPresent()) {
            PartnerDetailsBean partnerDetailsBean = getModelMapper().map(bean, PartnerDetailsBean.class);
            BeanUtils.copyProperties(partnerDetailsBean, partnerDetails.get(), "id", "dateCreated");
            partnerDetails.get().setCustomerId(id);
            update(partnerDetails.get().getId(), partnerDetails.get());
            if (Objects.nonNull(bean.getAttachments())) {
                customerAttachmentService.updateCustomerAttachment(id, bean.getAttachments());
            }
        }
        return getPartnerDetails(id);
    }

    @Override
    @Transactional
    public PartnerDetailsBean updateRowStatus(String id, Boolean status) {
        Customer customer = customerService.getRepository().getById(id);
        customer.setStatus(status);
        customerService.update(id, customer);
        return getPartnerDetails(id);
    }

    public Object deletePartner() {
        String partnerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        Optional<PartnerDetails> detailsOptional = ((PartnerDetailsRepository) getRepository()).findByCustomerId(partnerId);
        if (detailsOptional.isPresent()) {
            getRepository().delete(detailsOptional.get());
            Customer customer = customerService.getRepository().getById(partnerId);
            customer.setStatus(Boolean.FALSE);
            customerService.update(partnerId, customer);
            List<PartnerReview> partnerReviews = partnerReviewRepository.findByPartnerId(partnerId);
            partnerReviewRepository.deleteAll(partnerReviews);
            List<CustomerAttachment> attachments = ((CustomerAttachmentRepository) customerAttachmentService.getRepository()).findByCustomerId(partnerId);
            customerAttachmentService.getRepository().deleteAll(attachments);
            List<Transaction> transactions = transactionRepository.findByPartner(customer);
            transactionRepository.deleteAll(transactions);
            List<OrderInfo> orderInfos = orderRepository.findByPartnerId(partnerId);
            orderInfos.forEach(l -> l.setStatus(Boolean.FALSE));
            orderRepository.saveAll(orderInfos);
        }
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }
}

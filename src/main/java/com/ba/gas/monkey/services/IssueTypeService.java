package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.issuetype.IssueTypeBean;
import com.ba.gas.monkey.dtos.issuetype.IssueTypeListBean;
import com.ba.gas.monkey.models.IssueType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("issueTypeService")
public class IssueTypeService extends BaseService<IssueType, IssueTypeBean> {

    public IssueTypeService(BaseRepository<IssueType> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<IssueType> getCustomerIssueTypeList() {
        return getRepository().findAll();
    }


    public List<DropdownDTO> getDropdownList() {
        return getRepository().findAll().stream().filter(IssueType::getStatus)
                .map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(IssueType issueType) {
        DropdownDTO dto = getModelMapper().map(issueType, DropdownDTO.class);
        dto.setValue(issueType.getTitle());
        return dto;
    }

    public Object save(IssueTypeBean typeBean) {
        IssueType issueType = convertForCreate(typeBean);
        return create(issueType);
    }

    public Object updateType(String id, IssueTypeBean typeBean) {
        IssueType issueType = getRepository().getById(id);
        BeanUtils.copyProperties(typeBean, issueType, AppConstant.IGNORE_PROPERTIES);
        return update(id, issueType);
    }

    public Page<IssueTypeListBean> getIssueTypeList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title"));
        Page<IssueType> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private IssueTypeListBean getListBean(IssueType issueType) {
        IssueTypeListBean bean = getModelMapper().map(issueType, IssueTypeListBean.class);
        bean.setStatus(issueType.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}

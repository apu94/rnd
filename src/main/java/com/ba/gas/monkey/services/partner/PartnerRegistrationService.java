package com.ba.gas.monkey.services.partner;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.partner.PartnerCreateBean;
import com.ba.gas.monkey.dtos.partner.PartnerDetailsBean;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.models.PartnerDetails;
import com.ba.gas.monkey.services.customer.CustomerAttachmentService;
import com.ba.gas.monkey.services.customer.CustomerRegistrationService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import com.ba.gas.monkey.services.dealer.DealerBrandService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@AllArgsConstructor
@Service("partnerRegistrationService")
public class PartnerRegistrationService {

    private final ModelMapper modelMapper;
    private final CustomerRegistrationService customerRegistrationService;
    private final PartnerDetailsService partnerDetailsService;
    private final CustomerTypeService customerTypeService;
    private final CustomerAttachmentService customerAttachmentService;


    @Transactional
    public PartnerDetailsBean partnerRegistration(PartnerCreateBean bean) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        bean.setCustomerTypeId(customerType.getId());
        CustomerRegistrationBean customerRegistrationBean = modelMapper.map(bean, CustomerRegistrationBean.class);
        customerRegistrationBean.setApproved(Boolean.FALSE);
        customerRegistrationBean.setUserStatus(AppConstant.INACTIVE.toLowerCase());
        PartnerDetailsBean partnerDetailsBean = modelMapper.map(bean, PartnerDetailsBean.class);
        CustomerInfoBean customerRegistration = customerRegistrationService.createCustomerRegistration(customerRegistrationBean);
        partnerDetailsBean.setCustomerId(customerRegistration.getId());
        partnerDetailsBean.setStatus(customerRegistration.getStatus());
        PartnerDetails createdPartnerDetails = partnerDetailsService.savePartnerDetails(partnerDetailsBean);
        PartnerDetailsBean detailsBean = modelMapper.map(createdPartnerDetails, PartnerDetailsBean.class);
        detailsBean.setCustomerInfoBean(customerRegistration);
        detailsBean.setTotalEarnings(0.0);
        detailsBean.setTotalPayable(0.0);
        if (Objects.nonNull(bean.getAttachments())) {
            customerAttachmentService.createCustomerAttachment(detailsBean.getCustomerId(), bean.getAttachments());
        }
        return detailsBean;
    }


}

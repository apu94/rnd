package com.ba.gas.monkey.services;

import com.ba.gas.monkey.paylod.request.AppLogoutRequestBean;
import com.ba.gas.monkey.paylod.request.LogoutRequestBean;
import com.ba.gas.monkey.paylod.request.LogoutResponseBean;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
@AllArgsConstructor
@Service("logoutService")
public class LogoutService {

    private final DeviceTokenService deviceTokenService;
    public LogoutResponseBean logout(AppLogoutRequestBean request) {
        return deviceTokenService.removeDeviceToken(request);
    }

    public LogoutResponseBean adminLogout(LogoutRequestBean request) {
        return deviceTokenService.adminLogout(request);
    }
}

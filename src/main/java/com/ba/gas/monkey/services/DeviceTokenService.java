package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DeviceTokenBean;
import com.ba.gas.monkey.models.DeviceToken;
import com.ba.gas.monkey.paylod.request.AppLogoutRequestBean;
import com.ba.gas.monkey.paylod.request.LogoutRequestBean;
import com.ba.gas.monkey.paylod.request.LogoutResponseBean;
import com.ba.gas.monkey.repositories.DeviceTokenRepository;
import com.ba.gas.monkey.services.user.UserInfoService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service("deviceTokenService")
public class DeviceTokenService extends BaseService<DeviceToken, DeviceTokenBean> {

    private final UserInfoService userInfoService;

    public DeviceTokenService(BaseRepository<DeviceToken> repository, ModelMapper modelMapper, UserInfoService userInfoService) {
        super(repository, modelMapper);
        this.userInfoService = userInfoService;
    }

    public void saveDeviceToken(DeviceTokenBean deviceTokenBean) {
        List<DeviceToken> list = ((DeviceTokenRepository) getRepository())
                .findAllByUserIdAndDeviceType(deviceTokenBean.getUserId(), deviceTokenBean.getDeviceType());
        if (!list.isEmpty()) {
            getRepository().deleteAll(list);
        }
        DeviceToken deviceToken = convertForCreate(deviceTokenBean);
        deviceToken.setToken(deviceToken.getToken());
        deviceToken.setDateCreated(Instant.now());
        deviceToken.setStatus(Boolean.TRUE);
        deviceToken.setDateModified(Instant.now());
        deviceToken.setUpdtId(deviceToken.getUserId());
        getRepository().save(deviceToken);
    }

    public List<DeviceTokenBean> getDeviceTokenByUserId(String userId) {
        List<DeviceToken> tokens = ((DeviceTokenRepository) getRepository()).findByUserId(userId);
        return convertForRead(tokens);
    }

    public LogoutResponseBean removeDeviceToken(AppLogoutRequestBean request) {
        Optional<DeviceToken> optional = ((DeviceTokenRepository) getRepository())
                .findByUserIdAndDeviceType(request.getUserId(), request.getDeviceType());
        optional.ifPresent(deviceToken -> getRepository().delete(deviceToken));
        return LogoutResponseBean.builder().success(Boolean.TRUE).build();
    }

    public List<DeviceTokenBean> getAllAdminDeviceToken() {
        List<String> ids = userInfoService.getAllAdminIds();
        List<DeviceToken> tokens = ((DeviceTokenRepository) getRepository()).findByUserIds(ids);
        return convertForRead(tokens);
    }

    public List<DeviceToken> getDeviceTokenByUserIds(List<String> customerIds) {
        return ((DeviceTokenRepository) getRepository()).findByUserIds(customerIds);
    }

    public LogoutResponseBean adminLogout(LogoutRequestBean request) {
        List<DeviceToken> deviceTokens = ((DeviceTokenRepository) getRepository()).findByUserIds(Collections.singletonList(request.getUserId()));
        getRepository().deleteAll(deviceTokens);
        return LogoutResponseBean.builder().success(Boolean.TRUE).build();
    }

    public List<DeviceToken> getDeviceTokenByUserIdAndDevice(String userId, String deviceType) {
        return ((DeviceTokenRepository) getRepository()).findAllByUserIdAndDeviceType(userId, deviceType.toUpperCase());
    }
}

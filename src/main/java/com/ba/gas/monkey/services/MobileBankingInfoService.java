package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.mobilebanking.MobileBankingInfoBean;
import com.ba.gas.monkey.dtos.mobilebanking.MobileBankingInfoListBean;
import com.ba.gas.monkey.models.MobileBankingInfo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("mobileBankingInfoService")
public class MobileBankingInfoService extends BaseService<MobileBankingInfo, MobileBankingInfoBean> {

    public MobileBankingInfoService(BaseRepository<MobileBankingInfo> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }


    public Page<MobileBankingInfoListBean> getListBeans(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("status"), Sort.Order.asc("name")));
        Page<MobileBankingInfo> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private MobileBankingInfoListBean getListBean(MobileBankingInfo bankingInfo) {
        MobileBankingInfoListBean bean = getModelMapper().map(bankingInfo, MobileBankingInfoListBean.class);
        bean.setStatus(bankingInfo.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public List<DropdownDTO> getDropdownList() {
        List<MobileBankingInfo> list = getRepository().findAll(Sort.by("name"));
        return list.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    public DropdownDTO getDropdownDTO(MobileBankingInfo mobileBankingInfo) {
        DropdownDTO dto = getModelMapper().map(mobileBankingInfo, DropdownDTO.class);
        dto.setValue(mobileBankingInfo.getName());
        return dto;
    }

    public MobileBankingInfoBean saveMobileBankingInfo(MobileBankingInfoBean bean) {
        return super.create(convertForCreate(bean));
    }

    public Object updateMobileBankingInfo(String id, MobileBankingInfoBean bean) {
        MobileBankingInfo bankingInfo = getById(id);
        BeanUtils.copyProperties(bean, bankingInfo, AppConstant.IGNORE_PROPERTIES);
        return update(id, bankingInfo);
    }

    public Object deleteMobileBankingInfo(String id) {
        MobileBankingInfo bankingInfo = getRepository().getById(id);
        getRepository().delete(bankingInfo);
        return Boolean.TRUE;
    }
}

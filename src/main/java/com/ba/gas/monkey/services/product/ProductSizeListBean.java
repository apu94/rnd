package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class ProductSizeListBean extends RowInfoBean {
    private String nameEn;
    private String nameBn;
    private String status;
}

package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DeliveryScheduleBean;
import com.ba.gas.monkey.dtos.timeslot.TimeSlotBean;
import com.ba.gas.monkey.dtos.timeslot.TimeSlotRequestBean;
import com.ba.gas.monkey.models.DeliverySchedule;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;

@Service("deliveryScheduleService")
public class DeliveryScheduleService extends BaseService<DeliverySchedule, DeliveryScheduleBean> {

    public DeliveryScheduleService(BaseRepository<DeliverySchedule> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public DeliveryScheduleBean getData() {
        return convertForRead(getRepository().findAll().get(0));
    }

    public void updateData(DeliveryScheduleBean scheduleBean) {
        DeliverySchedule deliverySchedule = getRepository().getById(scheduleBean.getId());
        BeanUtils.copyProperties(scheduleBean, deliverySchedule, AppConstant.IGNORE_PROPERTIES);
        deliverySchedule.setId(scheduleBean.getId());
        getRepository().save(deliverySchedule);
    }

    public Object getTimeSlot() {
        List<DeliverySchedule> list = getRepository().findAll();
        if (list.isEmpty()) {
            return null;
        }
        String pattern = "hh:mm a";
        DeliverySchedule schedule = list.get(0);
//        LocalDate now = Instant.now().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDateTime currentDateTime = Instant.now().atZone(ZoneId.of(AppConstant.TIME_ZONE)).toLocalDateTime();
        LocalDateTime current = currentDateTime.withHour(schedule.getStartTime().getHour()).withMinute(schedule.getStartTime().getMinute());
        if (currentDateTime.isAfter(current)) {
            current = current.withHour(currentDateTime.getHour()).plusMinutes(schedule.getSlotDurationInMin());
        }
        LocalDateTime endTime = currentDateTime.withHour(schedule.getEndTime().getHour()).withMinute(schedule.getEndTime().getMinute());
        List<TimeSlotBean> times = new ArrayList<>();
        times.add(TimeSlotBean.builder().startTime(current.format(DateTimeFormatter.ofPattern(pattern)))
                .endTime(current.plusMinutes(schedule.getSlotDurationInMin()).format(DateTimeFormatter.ofPattern(pattern)))
                .build());
        while (current.isBefore(endTime)) {
            current = current.plusMinutes(schedule.getSlotDurationInMin());
            times.add(TimeSlotBean.builder().startTime(current.format(DateTimeFormatter.ofPattern(pattern)))
                    .endTime(current.plusMinutes(schedule.getSlotDurationInMin()).format(DateTimeFormatter.ofPattern(pattern)))
                    .build());
        }
        return times;
    }

    public Object getTimeSlotForSpecificDate(TimeSlotRequestBean requestBean) {
        List<DeliverySchedule> list = getRepository().findAll();
        if (list.isEmpty()) {
            return null;
        }
        String pattern = "hh:mm a";
        DeliverySchedule schedule = list.get(0);
        LocalDateTime currentDateTime = Instant.now().atZone(ZoneId.of(AppConstant.TIME_ZONE)).toLocalDateTime().withSecond(0);
        long between = DAYS.between(LocalDate.now(), requestBean.getDate());
        if (requestBean.getDate().isAfter(LocalDate.now()) && between > 0) {
            currentDateTime = Instant.now().atZone(ZoneId.of(AppConstant.TIME_ZONE)).toLocalDateTime()
                    .plusDays(between).withHour(schedule.getStartTime().getHour()).withMinute(schedule.getStartTime().getMinute()).withSecond(0);
        }
        LocalDateTime current = LocalDateTime.now(ZoneId.of(AppConstant.TIME_ZONE)).withMinute(schedule.getStartTime().getMinute());
        if (currentDateTime.isAfter(current)) {
            current = currentDateTime.withMinute(0);
        } else {
            current = current.withMinute(0).plusMinutes(schedule.getSlotDurationInMin());
        }

        LocalDateTime endTime = currentDateTime.withHour(schedule.getEndTime().getHour()).withMinute(schedule.getEndTime().getMinute()).withSecond(0);
        List<TimeSlotBean> times = new ArrayList<>();
        if (!requestBean.getDate().isEqual(LocalDate.now())) {
            times.add(TimeSlotBean.builder().startTime(current.format(DateTimeFormatter.ofPattern(pattern)))
                    .endTime(current.plusMinutes(schedule.getSlotDurationInMin()).format(DateTimeFormatter.ofPattern(pattern)))
                    .build());
        }
        while (current.plusMinutes(schedule.getSlotDurationInMin()).isBefore(endTime)) {
            current = current.plusMinutes(schedule.getSlotDurationInMin());
            times.add(TimeSlotBean.builder().startTime(current.format(DateTimeFormatter.ofPattern(pattern)))
                    .endTime(current.plusMinutes(schedule.getSlotDurationInMin()).format(DateTimeFormatter.ofPattern(pattern)))
                    .build());
        }
        return times;
    }
}

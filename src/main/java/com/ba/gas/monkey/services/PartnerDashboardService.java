package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.AverageTimeAndOrderDelivery;
import com.ba.gas.monkey.dtos.partner.PartnerDashboardInfoBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.PartnerReview;
import com.ba.gas.monkey.repositories.PartnerReviewRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.order.OrderService;
import com.ba.gas.monkey.services.partner.PartnerReviewService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service("partnerDashboardService")
public class PartnerDashboardService {

    private final CustomerService customerService;
    private final PartnerReviewService partnerReviewService;
    private final OrderService orderService;
    private final OrderHistoryService orderHistoryService;

    public PartnerDashboardInfoBean dashboardData() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        return PartnerDashboardInfoBean.builder()
                .customerBean(customerService.customerDetails(customer.getId()))
                .partnerDashboardBean(orderService.getPartnerDashboard(customer.getId()))
                .ratingPoint(ratingPoint(customer)).build();
    }

    private Double ratingPoint(Customer customer) {
        List<PartnerReview> list = ((PartnerReviewRepository) partnerReviewService.getRepository()).findByPartner(customer);
        List<Double> ratings = list.stream().map(PartnerReview::getReviewRating).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        return average.orElse(0.0);
    }

    public AverageTimeAndOrderDelivery homeDataAverageTimeAndOrderDelivery() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        AverageTimeAndOrderDelivery averageTimeAndOrderDelivery = new AverageTimeAndOrderDelivery();
        averageTimeAndOrderDelivery.setAverageTime(orderService.getAverageTime(customer));
        averageTimeAndOrderDelivery.setDelivery(orderService.getCompletedOrder(customer));
        averageTimeAndOrderDelivery.setOrder(orderHistoryService.getOrderList(customer).size());
        return averageTimeAndOrderDelivery;
    }

}

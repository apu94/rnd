package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.userstatus.UserStatusBean;
import com.ba.gas.monkey.dtos.customer.*;
import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import com.ba.gas.monkey.dtos.userstatus.UserStatusResponseBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.repositories.OrderRepository;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.DistrictService;
import com.ba.gas.monkey.services.ServiceChargeService;
import com.ba.gas.monkey.services.ThanaService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import com.ba.gas.monkey.utility.DateUtils;
import com.ba.gas.monkey.utility.PasswordUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service("customerService")
public class CustomerService extends BaseService<Customer, CustomerBean> {

    private final CustomerTypeService customerTypeService;
    private final DistrictService districtService;
    private final ThanaService thanaService;
    private final ClusterService clusterService;
    private final OrderRepository orderRepository;
    private final PasswordUtils passwordUtils;
    private final CustomerOrderDetailsService customerOrderDetailsService;
    private final ServiceChargeService serviceChargeService;

    public CustomerService(BaseRepository<Customer> repository, ModelMapper modelMapper,
                           CustomerTypeService customerTypeService,
                           DistrictService districtService,
                           ThanaService thanaService,
                           ClusterService clusterService,
                           PasswordUtils passwordUtils,
                           OrderRepository orderRepository,
                           ServiceChargeService serviceChargeService,
                           CustomerOrderDetailsService customerOrderDetailsService) {
        super(repository, modelMapper);
        this.customerTypeService = customerTypeService;
        this.districtService = districtService;
        this.thanaService = thanaService;
        this.clusterService = clusterService;
        this.passwordUtils = passwordUtils;
        this.orderRepository = orderRepository;
        this.serviceChargeService = serviceChargeService;
        this.customerOrderDetailsService = customerOrderDetailsService;
    }

    public Page<CustomerListBean> getCustomerListBeans(Pageable pageable) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.CUSTOMER_TYPE);
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "lastAccess"));
        Page<Customer> page = ((CustomerRepository) getRepository()).findByCustomerType(customerType, pageRequest);
        List<CustomerListBean> list = page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList());
        list.forEach(l -> {
            CustomerOrderInfoBean orderInfo = customerOrderInfo(l.getId());
            l.setRewardPoint(l.getRewardPoint());
            l.setLastOrder(DateUtils.getDateFromInstant(orderInfo.getLastOrder(), AppConstant.DATE_FORMAT_DD_MM_YYYY));
            l.setTotalOrderNo(orderInfo.getTotalOrder());
            l.setPurchaseHistory(orderInfo.getTotalPurchase());
            l.setPendingOrders(orderInfo.getPendingOrders());
        });
        AtomicInteger increment = new AtomicInteger(1);
        List<CustomerListBean> beans = list.stream().sorted(Comparator.comparing(CustomerListBean::getTotalOrderNo).reversed())
                .sorted(Comparator.comparing(CustomerListBean::getPendingOrders).reversed())
                .collect(Collectors.toUnmodifiableList());
        beans.forEach(b -> {
            b.setSlNo(increment.getAndIncrement());
        });
        return new PageImpl<>(beans, page.getPageable(), page.getTotalElements());
    }

    private CustomerOrderInfoBean customerOrderInfo(String id) {
        return customerOrderDetailsService.getOrderInfo(id);
    }

    public CustomerBean createCustomer(CustomerBean customerBean) {
        Customer customer = convertForCreate(customerBean);
        customer.setPassword(passwordUtils.getDefaultPassword());
        customer.setDistrict(districtService.getRepository().getById(customerBean.getDistrictId()));
        customer.setThana(thanaService.getRepository().getById(customerBean.getThanaId()));
        customer.setCluster(clusterService.getRepository().getById(customerBean.getClusterId()));
        customer.setCustomerType(customerTypeService.getRepository().getById(customerBean.getCustomerTypeId()));
        return create(customer);
    }

    public Optional<CustomerBean> getByPhoneNo(String phoneNo) {
        phoneNo = (phoneNo.length() > 11) ? phoneNo.substring(phoneNo.length() - 11) : phoneNo;
        Optional<Customer> optionalCustomer = ((CustomerRepository) getRepository()).findByPhoneNo(phoneNo);
        if (optionalCustomer.isEmpty()) {
            return Optional.empty();
        }
        CustomerBean customerBean = getModelMapper().map(optionalCustomer.get(), CustomerBean.class);
        List<String> authorities = Collections.singletonList(optionalCustomer.get().getCustomerType().getCustomerType());
        customerBean.setAuthorities(authorities);
        return Optional.of(customerBean);
    }


    public Optional<Customer> getAppUserByPhoneNo(String phoneNo) {
        phoneNo = (phoneNo.length() > 11) ? phoneNo.substring(phoneNo.length() - 11) : phoneNo;
        String finalPhoneNo = phoneNo;
        return ((CustomerRepository) getRepository()).findByPhoneNo(finalPhoneNo);
    }

    public LoggedInUserBean getCustomerByPhoneNo(String phoneNo) {
        phoneNo = (phoneNo.length() > 11) ? phoneNo.substring(phoneNo.length() - 11) : phoneNo;
        String finalPhoneNo = phoneNo;
        Optional<Customer> customer = ((CustomerRepository) getRepository()).findByPhoneNo(finalPhoneNo);
        if (customer.isEmpty()) {
            return null;
        }
        List<String> authorities = Collections.singletonList(customer.get().getCustomerType().getCustomerType());
        return LoggedInUserBean.builder()
                .id(customer.get().getId())
                .authorities(authorities)
                .email(customer.get().getPhoneNo())
                .password(customer.get().getPassword())
                .status(customer.get().getStatus())
                .build();
    }

    private CustomerListBean getListBean(Customer customer) {
        CustomerListBean bean = getModelMapper().map(customer, CustomerListBean.class);
        bean.setStatus(customer.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setCustomerType(customer.getCustomerType().getCustomerName());
        bean.setDistrict(customer.getDistrict().getName());
        bean.setThana(customer.getThana().getName());
        bean.setLastAccess(Objects.isNull(customer.getLastAccess()) ? LocalDateTime.ofInstant(customer.getDateModified(), ZoneOffset.UTC) : customer.getLastAccess());
        return bean;
    }

    public CustomerInfoBean registration(CustomerRegistrationBean bean) {
        Optional<CustomerBean> optional = getByPhoneNo(bean.getPhoneNo());
        Customer createdCustomer;
        Customer customer = optional.map(customerBean -> getRepository().getById(customerBean.getId())).orElseGet(() -> getModelMapper().map(bean, Customer.class));
        customer.setCustomerName(bean.getCustomerName());
        customer.setPhotoLink(null);
        customer.setStatus(AppConstant.ACTIVE_USER);
        customer.setDateCreated(Instant.now());
        customer.setDateModified(Instant.now());
        customer.setUpdtId(bean.getUpdtId());
        customer.setLastAccess(LocalDateTime.now());
        customer.setDistrict(districtService.getRepository().getById(bean.getDistrictId()));
        customer.setThana(thanaService.getRepository().getById(bean.getThanaId()));
        customer.setCluster(clusterService.getRepository().getById(bean.getClusterId()));
        customer.setCustomerType(customerTypeService.getRepository().getById(bean.getCustomerTypeId()));
        createdCustomer = getRepository().save(customer);
        return getModelMapper().map(createdCustomer, CustomerInfoBean.class);
    }

    public CustomerBean customerDetails(String customerId) {
        Customer customer = getRepository().getById(customerId);
        CustomerBean customerBean = getModelMapper().map(customer, CustomerBean.class);
        List<String> authorities = Collections.singletonList(customer.getCustomerType().getCustomerType());
        customerBean.setAuthorities(authorities);
        customerBean.setTypeBean(getModelMapper().map(customer.getCustomerType(), CustomerTypeBean.class));
        return customerBean;
    }

    public List<Customer> getNearestDistanceList(DistanceBean distanceBean) {
        return ((CustomerRepository) getRepository())
                .findWithInDistance(distanceBean.getId(), distanceBean.getLatitude(), distanceBean.getLongitude(), distanceBean.getDistanceInKM());
    }

    public Page<Customer> getNearestDistanceList(DistanceBean distanceBean, List<String> partnerIds, Pageable pageable) {
        if (partnerIds.isEmpty()) {
            return ((CustomerRepository) getRepository())
                    .findByDistance(distanceBean.getId(), distanceBean.getLatitude(), distanceBean.getLongitude(), distanceBean.getDistanceInKM(), pageable);
        }
        return ((CustomerRepository) getRepository())
                .findByDistanceFilterList(distanceBean.getId(), distanceBean.getLatitude(), distanceBean.getLongitude(), distanceBean.getDistanceInKM(), partnerIds, pageable);
    }

    public Page<Object[]> getAllNearestDistanceList(DistanceBean distanceBean, Pageable pageable) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.PARTNER_TYPE);
        Optional<ServiceCharge> optional = serviceChargeService.getByServiceType(AppConstant.SEARCH_AREA_IN_KM);
        Double distance = (optional.isPresent()) ? optional.get().getServiceValue() : AppConstant.DEFAULT_DISTANCE_FOR_SEARCH;
        distanceBean.setId(customerType.getId());
        distanceBean.setDistanceInKM(distance);
        return ((CustomerRepository) getRepository())
                .findAllByDistance(distanceBean.getId(), distanceBean.getLatitude(), distanceBean.getLongitude(), distanceBean.getDistanceInKM(), pageable);
    }

    public CustomerBean profileUpdate(ProfileUpdateBean bean) {
        Customer customer = getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        customer.setCustomerName((bean.getCustomerName() != null) ? bean.getCustomerName() : customer.getCustomerName());
        customer.setEmailAddress((bean.getEmailAddress() != null) ? bean.getEmailAddress() : customer.getEmailAddress());
        customer.setPhotoLink((bean.getPhotoLink() != null) ? bean.getPhotoLink() : customer.getPhotoLink());
        Customer updatedCustomer = getRepository().save(customer);
        return getByOid(updatedCustomer.getId());
    }

    public CustomerInfoBean profile() {
        Customer customer = getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        return getModelMapper().map(customer, CustomerInfoBean.class);
    }


    public void passwordUpdate(CustomerBean customerBean, ForgetPasswordBean passwordBean) {
        Customer customer = getRepository().getById(customerBean.getId());
        customer.setPassword(passwordUtils.getHashPassword(passwordBean.getPassword()));
        getRepository().save(customer);
    }

    public Object updateStatus(UserStatusBean userStatusBean) {
        Customer customer = getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        customer.setUserStatus(userStatusBean.getStatus() ? AppConstant.ACTIVE.toLowerCase() : AppConstant.INACTIVE.toLowerCase());
        getRepository().save(customer);
        return UserStatusResponseBean.builder().status(userStatusBean.getStatus())
                .userStatus(userStatusBean.getStatus() ? AppConstant.ONLINE : AppConstant.OFFLINE)
                .build();
    }

    public Object status() {
        Customer customer = getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        return UserStatusResponseBean.builder()
                .status(!customer.getUserStatus().equalsIgnoreCase(AppConstant.INACTIVE))
                .userStatus(customer.getUserStatus().equalsIgnoreCase(AppConstant.INACTIVE) ? AppConstant.OFFLINE : AppConstant.ONLINE)
                .build();
    }

    public Object updateLocation(UpdateLocationBean locationBean) {
        Customer customer = getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        customer.setLongitude(locationBean.getLongitude());
        customer.setLatitude(locationBean.getLatitude());
        customer.setDateModified(Instant.now());
        getRepository().save(customer);
        return UpdateLocationResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public CheckAvailabilityBean checkAvailability(String phoneNo) {
        phoneNo = (phoneNo.length() > 11) ? phoneNo.substring(phoneNo.length() - 11) : phoneNo;
        Optional<Customer> optional = ((CustomerRepository) getRepository()).findByPhoneNo(phoneNo);
        if (optional.isPresent()) {
            return CheckAvailabilityBean.builder().available(!optional.get().getStatus()).build();
        }
        return CheckAvailabilityBean.builder().available(Boolean.TRUE).build();
    }

    public Object getDropdownList() {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.CUSTOMER_TYPE);
        List<Customer> list = ((CustomerRepository) getRepository()).getByTypeId(customerType.getId());
        return list.stream().sorted(Comparator.comparing(Customer::getCustomerName))
                .map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Customer customer) {
        DropdownDTO dropdownDTO = getModelMapper().map(customer, DropdownDTO.class);
        dropdownDTO.setValue(customer.getCustomerName());
        return dropdownDTO;
    }

    public void updateRewardPoint(String id) {
        Customer customer = getRepository().getById(id);
        Integer rewardPoint = Optional.ofNullable(customer.getRewardPoint()).orElse(0);
        customer.setRewardPoint(rewardPoint + 1);
        getRepository().save(customer);
    }


    @Transactional
    public Object updateCustomer(String id, CustomerBean customerBean) {
        Customer customer = getRepository().getById(id);
        BeanUtils.copyProperties(customerBean, customer, "id", "approved", "userStatus", "customerType", "dateCreated", "password", "photoLink");
        customer.setCluster(clusterService.getRepository().getById(customerBean.getClusterId()));
        customer.setDistrict(districtService.getRepository().getById(customerBean.getDistrictId()));
        customer.setThana(thanaService.getRepository().getById(customerBean.getThanaId()));
        CustomerBean updatedCustomer = update(id, customer);
        return getCustomerInfoById(updatedCustomer.getId());
    }

    public CustomerDetailsInfoBean getCustomerInfoById(String id) {
        Customer customer = getRepository().getById(id);
        CustomerOrderInfoBean orderInfo = customerOrderInfo(id);
        CustomerDetailsInfoBean bean = getModelMapper().map(customer, CustomerDetailsInfoBean.class);
        bean.setCustomerTypeId(customer.getCustomerType().getId());
        bean.setPendingOrder(orderInfo.getPendingOrders());
        bean.setCancelledOrder(orderInfo.getCancelledOrders());
        bean.setTotalDelivered(orderInfo.getTotalDelivered());
        bean.setTotalOrder(orderInfo.getTotalOrder());
        bean.setLastAccess(Objects.isNull(customer.getLastAccess()) ? LocalDateTime.ofInstant(customer.getDateModified(), ZoneOffset.UTC) : customer.getLastAccess());
        bean.setOrderDetailsBeans(getOrderDetailsBeans(id));
        bean.setCustomerTypeBean(getModelMapper().map(customer.getCustomerType(), CustomerTypeBean.class));
        return bean;
    }

    private List<CustomerOrderDetailsBean> getOrderDetailsBeans(String id) {
        List<OrderInfo> orderInfos = orderRepository.findByCustomerId(id);
        List<CustomerOrderDetailsBean> beans = orderInfos.stream().map(this::getOrderDetailsBean).collect(Collectors.toUnmodifiableList());
        AtomicInteger increment = new AtomicInteger(1);
        beans.forEach(b -> {
            b.setSlNo(increment.getAndIncrement());
        });
        return beans;
    }

    private CustomerOrderDetailsBean getOrderDetailsBean(OrderInfo orderInfo) {
        List<String> orderTypes = new ArrayList<>();
        List<String> productName = new ArrayList<>();
        List<String> returnProducts = new ArrayList<>();
        for (OrderProduct orderProduct : orderInfo.getOrderProductList()) {
            productName.add(orderProduct.getBuyProduct().getBrand().getNameEn());
            Optional<Product> optional = Optional.ofNullable(orderProduct.getReturnProduct());
            optional.ifPresent(f -> returnProducts.add(f.getBrand().getNameEn()));
            orderTypes.add(optional.isEmpty() ? AppConstant.ORDER_TYPE_PACKAGE : AppConstant.ORDER_TYPE_REFILL);
        }
        return CustomerOrderDetailsBean.builder()
                .orderType(String.join(",", orderTypes))
                .productName(String.join(",", productName))
                .emptyCylinderBrandName(String.join(",", returnProducts))
                .orderNumber(String.valueOf(orderInfo.getOrderNumber()))
                .deliveryStatus(orderInfo.getOrderStatus())
                .totalAmount(orderInfo.getTotal())
                .build();
    }

    public Object deleteCustomer(String customerId) {
        Customer customer = getRepository().getById(customerId);
        customer.setStatus(Boolean.FALSE);
        update(customerId, customer);
        List<OrderInfo> orderInfos = orderRepository.findByCustomerId(customerId, Sort.by(Sort.Order.desc("dateCreated")));
        orderInfos.forEach(l -> l.setStatus(Boolean.FALSE));
        orderRepository.saveAll(orderInfos);
        return CommonResponseBean.builder().msg(AppConstant.ACCOUNT_DELETE_MSG).build();
    }


    public List<Customer> getNearestDealerList(DistanceBean distanceBean) {
        return ((CustomerRepository) getRepository())
                .findAllActiveDealer(distanceBean.getId(), distanceBean.getLatitude(), distanceBean.getLongitude());
    }

    public void updateLastAccess(String id) {
        Customer customer = getRepository().getById(id);
        customer.setLastAccess(LocalDateTime.now());
        getRepository().save(customer);
    }

}

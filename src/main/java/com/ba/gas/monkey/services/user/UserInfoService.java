package com.ba.gas.monkey.services.user;

import com.ba.gas.monkey.dtos.CheckAvailabilityBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.dtos.user.UserInfoEditBean;
import com.ba.gas.monkey.dtos.user.UserListBean;
import com.ba.gas.monkey.models.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.Optional;

public interface UserInfoService {

    Page<UserListBean> getList(Pageable pageable);

    Optional<UserInfoBean> getByEmailAddress(String emailAddress);

    Optional<UserInfo> getById(String userId);

    UserInfoBean getByUserId(String userId);

    UserInfoBean getUserDetails(String userId);

    Optional<UserInfoBean> updateUser(String id, UserInfoEditBean userInfoBean);

    void update(UserInfo userInfo);

    List<String> getAllAdminIds();

    CheckAvailabilityBean checkMobileAvailability(String phoneNo);
    CheckAvailabilityBean checkEmailAvailability(@Email String email);

    UserInfo forgetPassword(String email, String newPassword);
    UserInfo userByEmail(String email);

    void updateLastAccess(String id);
}

package com.ba.gas.monkey.services.partner;

import com.ba.gas.monkey.base.BaseEntity;
import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.customer.CustomerBasicInfoBean;
import com.ba.gas.monkey.dtos.partner.PartnerReviewBean;
import com.ba.gas.monkey.dtos.partner.PartnerReviewListBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.PartnerReview;
import com.ba.gas.monkey.repositories.PartnerReviewRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.order.OrderService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("partnerReviewService")
public class PartnerReviewService extends BaseService<PartnerReview, PartnerReviewBean> {

    private final CustomerService customerService;
    private final OrderService orderService;

    public PartnerReviewService(BaseRepository<PartnerReview> repository, ModelMapper modelMapper,
                                CustomerService customerService, OrderService orderService) {
        super(repository, modelMapper);
        this.customerService = customerService;
        this.orderService = orderService;
    }

    public Object saveReview(PartnerReviewBean bean) {
        String customerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        PartnerReview partnerReview = convertForCreate(bean);
        partnerReview.setCustomer(customerService.getRepository().getById(customerId));
        partnerReview.setPartner(customerService.getRepository().getById(bean.getPartnerId()));
        partnerReview.setOrderInfo(orderService.getRepository().getById(bean.getOrderId()));
        partnerReview.setReviewDate(LocalDateTime.now());
        partnerReview.setDateCreated(Instant.now());
        partnerReview.setDateModified(Instant.now());
        partnerReview.setUpdtId(customerId);
        partnerReview.setStatus(Boolean.TRUE);
        partnerReview.setReviewRead(Boolean.FALSE);
        return convertForRead(getRepository().save(partnerReview));
    }

    public Object getRatingDetails(String partnerId) {
        List<PartnerReview> reviews = ((PartnerReviewRepository) getRepository()).findByPartnerId(partnerId);
        return reviews.stream().filter(BaseEntity::getStatus).map(this::getBasicInfoBean)
                .sorted(Comparator.comparing(PartnerReviewListBean::getReviewDate).reversed()).collect(Collectors.toUnmodifiableList());
    }

    private PartnerReviewListBean getBasicInfoBean(PartnerReview partnerReview) {
        PartnerReviewListBean bean = getModelMapper().map(partnerReview, PartnerReviewListBean.class);
        bean.setCustomerBasicInfoBean(getModelMapper().map(partnerReview.getCustomer(), CustomerBasicInfoBean.class));
        bean.setOrderLocation(partnerReview.getOrderInfo().getDeliveryMapAddress());
        bean.setCluster(partnerReview.getOrderInfo().getCluster().getName());
        return bean;
    }
}

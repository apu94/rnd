package com.ba.gas.monkey.web;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.UserEmailBean;
import com.ba.gas.monkey.dtos.otp.OTPBean;
import com.ba.gas.monkey.dtos.otp.OTPRequestBean;
import com.ba.gas.monkey.dtos.otp.OTPSendRequestBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.paylod.request.LogoutRequestBean;
import com.ba.gas.monkey.paylod.response.ResponseBean;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.PasswordUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

@Slf4j
@Service("webService")
public class WebService {

    @Value("${access.token.url}")
    private String accessTokenURL;

    @Value("${access.token.clientId}")
    private String keyClockClient;

    @Value("${access.token.secretKey}")
    private String keyClockClientSecret;

    @Value("${broker.service.url}")
    private String brokerServiceURL;

    @Value("${sender.email.address}")
    private String senderEmailAddress;

    private final WebClient webClientInstance;
    private final ObjectMapper objectMapper;
    private final UserInfoService userInfoService;
    private final CustomerService customerService;
    private final PasswordUtils passwordUtils;


    WebService(WebClient webClientInstance, ObjectMapper objectMapper,
               UserInfoService userInfoService,
               CustomerService customerService,
               PasswordUtils passwordUtils) {
        this.webClientInstance = webClientInstance;
        this.objectMapper = objectMapper;
        this.userInfoService = userInfoService;
        this.passwordUtils = passwordUtils;
        this.customerService = customerService;
    }


    private void keyCloakTokenRequest() {
        String requestData = "client_id=" + URLEncoder.encode(keyClockClient, StandardCharsets.UTF_8)
                + "&client_secret=" + URLEncoder.encode(keyClockClientSecret, StandardCharsets.UTF_8)
                + "&grant_type=" + URLEncoder.encode("client_credentials", StandardCharsets.UTF_8);
        log.info("access-token-URL ======>>>> " + accessTokenURL);
        log.info("Token-request-data ======>>>> " + requestData);
        JsonNode responseNode = webClientInstance.post()
                .uri(accessTokenURL)
                .body(BodyInserters.fromValue(requestData))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> Mono.error(new ServiceExceptionHolder.CustomException(AppConstant.KEY_CLOAK_TOKEN_EXCEPTION, clientResponse.toString())))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> Mono.error(new ServiceExceptionHolder.CustomException(AppConstant.KEY_CLOAK_TOKEN_EXCEPTION, clientResponse.toString())))
                .bodyToMono(JsonNode.class).block();
        assert responseNode != null;
        AppConstant.SESSION_DATA_MAP.put(AppConstant.KEY_CLOAK_TOKEN, responseNode);
        AppConstant.SESSION_DATA_MAP.put(AppConstant.KEY_CLOAK_TOKEN_EXPIRY, LocalDateTime.now().plusSeconds(280));
    }

    private String getKeyCloakToken() {
        if (!AppConstant.SESSION_DATA_MAP.containsKey(AppConstant.KEY_CLOAK_TOKEN)) {
            keyCloakTokenRequest();
        }
        LocalDateTime expiryTime = (LocalDateTime) AppConstant.SESSION_DATA_MAP.get(AppConstant.KEY_CLOAK_TOKEN_EXPIRY);
        if (expiryTime.isBefore(LocalDateTime.now())) {
            keyCloakTokenRequest();
        }
        JsonNode tokenJson = (JsonNode) AppConstant.SESSION_DATA_MAP.get(AppConstant.KEY_CLOAK_TOKEN);
        return tokenJson.get("access_token").asText();
    }

    public OTPBean sendOTP(OTPRequestBean requestBean) {
//        Optional<CustomerBean> optional = customerService.getByPhoneNo(requestBean.getPhoneNo());
//        if (optional.isEmpty()) {
//            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "No user found");
//        }
        JsonNode response = webClientInstance.post()
                .uri(brokerServiceURL + "/api/broker-service/otp/send_otp")
                .body(BodyInserters.fromValue(getSmsSendRequestBean(requestBean.getPhoneNo())))
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getKeyCloakToken())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                })).onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                }))
                .bodyToMono(JsonNode.class).block();
        assert response != null;
        String opt = response.get("data").get("otp").asText();
        OTPBean otpBean = OTPBean.builder()
                .expiryTime(Instant.now().plusSeconds(60)).otp(opt)
                .msg(AppConstant.SUCCESS)
                .sessionId(requestBean.getSessionId())
                .phoneNo(requestBean.getPhoneNo()).build();
        AppConstant.SESSION_DATA_MAP.put(requestBean.getSessionId() + "#" + requestBean.getPhoneNo(), otpBean);
        log.info("server-response {}", otpBean);
        return otpBean;
    }

    private OTPSendRequestBean getSmsSendRequestBean(String phoneNo) {
        return OTPSendRequestBean
                .builder()
                .destination(phoneNo)
                .msgTmp(AppConstant.OTP_TEMPLATE)
                .otpType(AppConstant.OTP_TYPE_SMS)
                .otpLength(AppConstant.OTP_LENGTH)
                .build();
    }

    public void logTraceResponse(WebClientResponseException exception) {
        log.info("Response status: {}", exception.getRawStatusCode());
        log.info("Response headers: {}", exception.getHeaders());
        log.info("Response body: {}", exception.getResponseBodyAsString());
    }

    public JsonNode createMessageForException(String response) {
        try {
            return objectMapper.readTree(response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String sendEmail(UserEmailBean userEmailBean) {
        String newPassword = passwordUtils.randomPassword();
        UserInfo userInfo = userInfoService.userByEmail(userEmailBean.getEmail());
        JsonNode jsonNode = webClientInstance.post()
                .uri(brokerServiceURL + "/api/broker-service/email/send_email")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData(getEmailOptRequestDTO(userInfo, newPassword)))
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getKeyCloakToken())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                })).onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                })).bodyToMono(JsonNode.class).block();
        return newPassword;
    }

    private MultiValueMap<String, String> getEmailOptRequestDTO(UserInfo userInfo, String newPassword) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("receipant", userInfo.getEmail());
        formData.add("subject", "Reset Password");
        formData.add("bodyHtml", "Your new password is " + newPassword);
        formData.add("sender", senderEmailAddress);
        return formData;
    }


    public Object forgetPassword(UserEmailBean userEmailBean) {
        String password = sendEmail(userEmailBean);
        userInfoService.forgetPassword(userEmailBean.getEmail(), passwordUtils.getHashPassword(password));
        return ResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Object sendLoginOTP(OTPRequestBean requestBean) {
        Optional<CustomerBean> optional = customerService.getByPhoneNo(requestBean.getPhoneNo());
        if (optional.isEmpty() || !optional.get().getStatus()) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "No user found");
        }
        JsonNode response = webClientInstance.post()
                .uri(brokerServiceURL + "/api/broker-service/otp/send_otp")
                .body(BodyInserters.fromValue(getSmsSendRequestBean(requestBean.getPhoneNo())))
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + getKeyCloakToken())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                })).onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.createException().map(ex -> {
                    logTraceResponse(ex);
                    return ex;
                }))
                .bodyToMono(JsonNode.class).block();
        assert response != null;
        String opt = response.get("data").get("otp").asText();
        OTPBean otpBean = OTPBean.builder()
                .expiryTime(Instant.now().plusSeconds(60)).otp(opt)
                .msg(AppConstant.SUCCESS)
                .sessionId(requestBean.getSessionId())
                .phoneNo(requestBean.getPhoneNo()).build();
        AppConstant.SESSION_DATA_MAP.put(requestBean.getSessionId() + "#" + requestBean.getPhoneNo(), otpBean);
        log.info("server-response {}", otpBean);
        return otpBean;
    }
}

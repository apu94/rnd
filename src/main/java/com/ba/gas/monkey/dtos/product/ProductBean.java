package com.ba.gas.monkey.dtos.product;
import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.ProductImage;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.models.ProductValveSize;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.util.List;
@Data
public class ProductBean implements IRequestBodyDTO {
    private String id;
//    @NotBlank(message = "Name can't be null")
//    private String nameEn;
//    @NotBlank(message = "Name can't be null")
//    private String nameBn;
//    @NotBlank(message = "Description  can't be null")
//    private String descriptionEn;
//    @NotBlank(message = "Description can't be null")
//    private String descriptionBn;
    @ValidEntityId(Brand.class)
    private String brandId;
    @ValidEntityId(ProductSize.class)
    private String productSizeId;
    @ValidEntityId(ProductValveSize.class)
    private String productValveSizeId;
    private Boolean status;
    //for update
    private Boolean featureProduct;
    private Boolean offerProduct;
    private List<ProductImage> productImageList;
}

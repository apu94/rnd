package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Cart;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class CartDetailBeanRequest {
    private String id;
    @ValidEntityId(Product.class)
    private String buyProduct;
    private String returnProduct;
    private Boolean status;
    private Integer quantity;
}

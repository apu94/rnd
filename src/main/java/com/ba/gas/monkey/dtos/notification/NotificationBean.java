package com.ba.gas.monkey.dtos.notification;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import com.ba.gas.monkey.models.NotificationDetails;
import lombok.Data;


@Data
public class NotificationBean extends RowInfoBean implements IRequestBodyDTO {
    private String templateId;
    private String receiverId;
    private String module;
    private String deviceToken;
    private String destination;
    private Integer notificationStatus;
    private String mediaType;
    private String notificationType;
    private Boolean seen;
    private Integer noOfTry;
    private String deviceType;
    private Boolean status;
    private NotificationDetails notificationDetails;
}

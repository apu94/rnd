package com.ba.gas.monkey.dtos.issuetype;

import lombok.Data;

@Data
public class IssueTypeListBean {
    private String id;
    private String title;
    private String description;
    private String status;
}

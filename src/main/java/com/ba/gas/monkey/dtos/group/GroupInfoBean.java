package com.ba.gas.monkey.dtos.group;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import javax.persistence.Column;


@Data
public class GroupInfoBean implements IRequestBodyDTO {

    private String id;
    private String groupName;
    private String groupType;

}

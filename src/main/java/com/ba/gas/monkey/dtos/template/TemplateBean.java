package com.ba.gas.monkey.dtos.template;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class TemplateBean implements IRequestBodyDTO {
    private String id;
    private String title;
    private String description;
    private String appToUser;
    private Boolean status;
}

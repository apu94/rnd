package com.ba.gas.monkey.dtos.userstatus;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class UserStatusResponseBean {
    private Boolean status;
    private String userStatus;
}

package com.ba.gas.monkey.dtos.notification;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;


@Data
public class NotificationTypeBean implements IRequestBodyDTO {
    private String name;
}

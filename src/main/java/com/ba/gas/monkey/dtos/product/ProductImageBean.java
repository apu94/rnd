package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class ProductImageBean implements IRequestBodyDTO {
    private String id;
    @NotBlank
    private String imageLink;
    @NotNull
    private Boolean status;
    @ValidEntityId(Product.class)
    private String productId;
    @NotBlank
    private String title;
    private String imageLinkId;
}

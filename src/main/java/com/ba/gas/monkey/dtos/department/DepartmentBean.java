package com.ba.gas.monkey.dtos.department;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class DepartmentBean extends RowInfoBean implements IRequestBodyDTO {
    private String name;
    private Boolean status;
}

package com.ba.gas.monkey.dtos.otp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class OTPSendRequestBean {
    private String msgTmp;
    private String destination;
    private String otpType;
    private Integer otpLength;
}

package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.*;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderHistoryBean implements IRequestBodyDTO {
    private String id;
    private String orderStatus;
    private Customer partner;
    private OrderInfo orderInfo;
    private Boolean status;
}

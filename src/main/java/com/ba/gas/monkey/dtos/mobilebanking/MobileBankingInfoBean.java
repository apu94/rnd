package com.ba.gas.monkey.dtos.mobilebanking;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MobileBankingInfoBean extends RowInfoBean implements IRequestBodyDTO {
    @NotBlank
    private String name;
    private Boolean status;
}

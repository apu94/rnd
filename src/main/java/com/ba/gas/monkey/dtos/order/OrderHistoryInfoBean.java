package com.ba.gas.monkey.dtos.order;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class OrderHistoryInfoBean {
    private String orderStatus;
    private String orderAction;
    private String response;
    private Instant actionTime;
}

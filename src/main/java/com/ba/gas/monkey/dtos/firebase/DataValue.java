package com.ba.gas.monkey.dtos.firebase;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;

@lombok.Data
public class DataValue implements IRequestBodyDTO {
    private String message;
}

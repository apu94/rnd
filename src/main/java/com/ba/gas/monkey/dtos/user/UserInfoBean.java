package com.ba.gas.monkey.dtos.user;

import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.dtos.designation.DesignationBean;
import com.ba.gas.monkey.dtos.group.GroupInfoBean;
import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Designation;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Data
public class UserInfoBean {

    private String id;
    private String userFullName;
    private DepartmentBean department;
    private DesignationBean designation;
    private String email;
    private String mobile;
    @JsonIgnore
    private String password;
    private LocalDateTime lastAccess;
    private String resetCredentialsReq;
    private LocalDate resetCredentialsExp;
    private GroupInfoBean groupInfoBean;
    private Boolean status;
    @JsonIgnore
    private List<String> authorities;
    @JsonIgnore
    private List<String> menus;
}

package com.ba.gas.monkey.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DistanceBean {
    @JsonIgnore
    private String id;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    private Double distanceInKM;
}

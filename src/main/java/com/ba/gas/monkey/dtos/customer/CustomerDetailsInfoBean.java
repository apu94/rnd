package com.ba.gas.monkey.dtos.customer;

import lombok.Data;

import java.util.List;

@Data
public class CustomerDetailsInfoBean extends CustomerInfoBean {
    private Integer totalOrder;
    private Long totalDelivered;
    private Long pendingOrder;
    private Long cancelledOrder;
    //    private Integer totalOrder;
    private List<CustomerOrderDetailsBean> orderDetailsBeans;
}

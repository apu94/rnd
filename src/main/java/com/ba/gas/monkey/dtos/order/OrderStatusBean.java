package com.ba.gas.monkey.dtos.order;

import lombok.Data;

@Data
public class OrderStatusBean {
    private String status;
}

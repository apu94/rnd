package com.ba.gas.monkey.dtos.notification;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;


@Data
public class NotificationDetailsBean implements IRequestBodyDTO {
    private String subject;
    private String content;
}

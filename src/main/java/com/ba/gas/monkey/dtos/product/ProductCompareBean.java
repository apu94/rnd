package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.models.ProductValveSize;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class ProductCompareBean {
    @ValidEntityId(Brand.class)
    private String brandId;
    @ValidEntityId(ProductSize.class)
    private String productSizeId;
    @ValidEntityId(ProductValveSize.class)
    private String productValveSizeId;
}

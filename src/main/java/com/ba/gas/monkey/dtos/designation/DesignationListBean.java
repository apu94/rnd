package com.ba.gas.monkey.dtos.designation;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class DesignationListBean extends RowInfoBean implements IRequestBodyDTO {
    private String name;
    private String status;
}

package com.ba.gas.monkey.dtos.brand;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BrandImageListBean {
    private String id;
    private String imageLink;
    private String status;
    private String title;
    private String imageLinkId;
}

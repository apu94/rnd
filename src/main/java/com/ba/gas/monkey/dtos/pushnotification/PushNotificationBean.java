package com.ba.gas.monkey.dtos.pushnotification;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.models.Template;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PushNotificationBean implements IRequestBodyDTO {
    @NotBlank
    private String subject;
    @ValidEntityId(CustomerType.class)
    private String typeId;
    @ValidEntityId(Template.class)
    private String templateId;
}

package com.ba.gas.monkey.dtos.firebase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionMaster implements Serializable {
    private String notificationId;
    private String token;
    private String type;
    private String deviceType;
    private Integer status;
    private Note note;
}

package com.ba.gas.monkey.dtos.user;

import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Designation;
import com.ba.gas.monkey.models.GroupInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Data
public class UserInfoEditBean {
    private String id;
    private String userFullName;
    @ValidEntityId(Department.class)
    private String departmentId;
    @ValidEntityId(Designation.class)
    private String designationId;
    private String email;
    private String mobile;
    @ValidEntityId(GroupInfo.class)
    private String groupId;
    @NotNull
    private Boolean status;
}

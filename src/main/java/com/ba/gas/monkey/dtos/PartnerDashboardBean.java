package com.ba.gas.monkey.dtos;

import lombok.Data;

import java.time.Month;
import java.util.Map;

@Data
public class PartnerDashboardBean implements IRequestBodyDTO {
    private Double totalEarning;
    private Double lastOrderEarnings;
    private Double monthlyEarning;
    private Double weeklyEarning;
    private Integer rewardPoint;
    private Double payable;
    Map<Month, Long> actualByMonth;
    Map<Month, Integer> calculatedByMonth;
}

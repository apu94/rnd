package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class PartnerDetailsListBean {
    private CustomerInfoBean customerInfoBean;
    @JsonIgnore
    private String customerId;
    private String workingZone;
    private String mobileBankName;
    private String mobileWalletNo;
    private String nomineeName;
    private String nomineePhone;
    private String nomineeRelationShip;
    private String status;
    private String approved;
    private Double depositBalance;
    private Double rating;
    private Boolean currentStatus;
}

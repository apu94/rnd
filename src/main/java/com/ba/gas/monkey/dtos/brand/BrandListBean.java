package com.ba.gas.monkey.dtos.brand;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BrandListBean {
    private String id;
    private String nameEn;
    private String nameBn;
    private String companyName;
    private String shortName;
    private String status;
    private String discountEnabled;
    private String discountType;
    private Double discountValue;
    private LocalDate discountStartDate;
    private LocalDate discountEndDate;
    private String imageLink;
}

package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class ProductPriceBean implements IRequestBodyDTO {
    private String id;
    private Double refillPrice;
    private Double packagePrice;
    private Double emptyCylinderPrice;
    private Boolean status;
}

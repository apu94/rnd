package com.ba.gas.monkey.dtos.reportissue;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.issuetype.IssueTypeBean;
import lombok.Data;

import java.time.Instant;

@Data
public class ReportIssueListBean  {
    private String id;
    private String ticketNo;
    private Integer issueStatus;
    private String details;
    private String note;
    private IssueTypeBean issueType;
    private CustomerBean customer;
    private String status;
    private Instant createdDate;
}

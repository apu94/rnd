package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class CartBean implements IRequestBodyDTO{
    private String id;
    private Customer customer;
    private Boolean status;
    private List<CartDetail> cartDetailList;
}

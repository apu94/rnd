package com.ba.gas.monkey.dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ServiceDiscountBean implements IRequestBodyDTO {
    private Boolean discountEnable;
    private String discountType;
    private Double discountValue;
    private Boolean status;
    private LocalDate discountStartDate;
    private LocalDate discountEndDate;
}

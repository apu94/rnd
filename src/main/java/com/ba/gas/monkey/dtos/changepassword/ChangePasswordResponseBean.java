package com.ba.gas.monkey.dtos.changepassword;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChangePasswordResponseBean {
    private String msg;
    private Boolean success;
}

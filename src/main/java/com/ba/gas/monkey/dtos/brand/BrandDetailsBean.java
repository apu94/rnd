package com.ba.gas.monkey.dtos.brand;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class BrandDetailsBean {

    private String id;
    private String nameEn;
    private String nameBn;
    private Boolean status;
    private String companyName;
    private String shortName;
    private Boolean discountEnabled;
    private String discountType;
    private Double discountValue;
    private LocalDate discountStartDate;
    private LocalDate discountEndDate;
    private List<BrandImageListBean> brandImages;
}

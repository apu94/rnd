package com.ba.gas.monkey.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class UserEmailBean {
    @Email
    @NotNull
    private String email;
}

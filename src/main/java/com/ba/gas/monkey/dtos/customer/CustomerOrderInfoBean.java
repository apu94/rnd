package com.ba.gas.monkey.dtos.customer;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;

@Data
@Builder
public class CustomerOrderInfoBean {
    private Integer totalOrder;
    private Double totalPurchase;
    private Instant lastOrder;
    private Long pendingOrders;
    private Long cancelledOrders;
    private Long totalDelivered;
}

package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.attachment.AttachmentInfoBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.validators.PhoneNumberValidation;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class PartnerCreateBean {
    private String id;
    @NotBlank
    private String customerName;
    @Email
    private String emailAddress;
    @PhoneNumberValidation
    private String phoneNo;
    @JsonIgnore
    private String customerTypeId;
    private String companyName;
    private Boolean status = Boolean.TRUE;
    @ValidEntityId(District.class)
    private String districtId;
    @ValidEntityId(Thana.class)
    private String thanaId;
    @ValidEntityId(Cluster.class)
    private String clusterId;
    private String area;
    private String address;
    private String gpsAddress;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    private String floorNo;
    private Boolean liftAllowed;
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;
    //partner-info
    @NotBlank
    private String workingZone;
    @ValidEntityId(MobileBankingInfo.class)
    private String mobileBankId;
    @NotBlank
    private String mobileWalletNo;
    @NotBlank
    private String nomineeName;
    @NotBlank
    private String nomineePhone;
    @NotBlank
    private String nomineeRelationShip;
    List<AttachmentInfoBean> attachments;
    private Integer rewardPoint = 0;
    private Double totalEarnings = 0.0;
    private Double totalPayable = 0.0;
}

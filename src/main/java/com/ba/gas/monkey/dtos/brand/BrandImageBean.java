package com.ba.gas.monkey.dtos.brand;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BrandImageBean implements IRequestBodyDTO {

    private String id;
    @NotBlank
    private String imageLink;
    @NotNull
    private Boolean status;
    @ValidEntityId(Brand.class)
    private String brandId;
    @NotBlank
    private String title;
    private String imageLinkId;
}

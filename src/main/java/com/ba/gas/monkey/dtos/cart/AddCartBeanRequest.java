package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.CartDetail;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class AddCartBeanRequest {
    @ValidEntityId(Customer.class)
    private String customerId;
    private CartDetailBeanRequest cartDetailBeanRequest;
}

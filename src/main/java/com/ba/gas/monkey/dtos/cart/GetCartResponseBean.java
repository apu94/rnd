package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.CartDetail;
import com.ba.gas.monkey.models.Customer;
import lombok.Data;

import java.util.List;

@Data
public class GetCartResponseBean implements IRequestBodyDTO {
    private String id;
    private Customer customer;
    private Boolean status;
    private List<CartDetail> cartDetailList;
    private Double subTotal = 0.0;
    private Double vat = 0.0;
    private Double serviceCharge = 0.0;
    private Double expressServiceCharge = 0.0;
    private Double discountAmount = 0.0;
    private Double total = 0.0;
    private Double vatPercent = 0.0;
    private Double exchange = 0.0;
    private Double totalExchange = 0.0;
    private Double perFloorCharge = 0.0;
    private Double floorNoCharge = 0.0;
    private Double totalConvenienceFee = 0.0;
    private Double couponAmount = 0.0;
    private String couponCode;
}

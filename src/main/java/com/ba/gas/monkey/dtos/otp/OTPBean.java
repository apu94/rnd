package com.ba.gas.monkey.dtos.otp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.time.LocalTime;

@Data
@Builder
public class OTPBean {
    private String phoneNo;
//    @JsonIgnore
    private Instant expiryTime;
//    @JsonIgnore
    private String otp;
    private String msg;
    private String sessionId;
}

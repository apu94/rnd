package com.ba.gas.monkey.dtos.appmenu;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AssignMenuResponseBean {
    private List<AssignMenuBean> menuBeans;
}
